"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var acteurs_1 = require("./acteurs");
var utils_1 = require("../utils/utils");
var utils_2 = require("../utils/utils");
/**
 * Classe mère permettant de faire hériter les classes filles Livre, CD, DVD
 *
 * @export
 * @abstract - Cette classe abstraite ne peut être instanciée directement
 * @class Media
 * @implements {Identifiable}
 */
var Media = /** @class */ (function () {
    //L'idEnregistré est fourni quand on crée un Media suite au chargement des données de l'application avec les MediaConverteur
    function Media(_idEnregistre, _type, _titre, _nbExemplaires, _indexPourRecherche) {
        if (_idEnregistre === void 0) { _idEnregistre = 0; }
        this.id = _idEnregistre ? _idEnregistre : utils_1.generateUniqueId();
        this.type = _type;
        this.titre = _titre;
        this.nbExemplaires = _nbExemplaires;
        this.indexPourRecherche = _indexPourRecherche;
    }
    /*
     * Retourne la description d'un média, méthode surchargée dans les classes filles Cd, DVD, Livre
     */
    Media.prototype.getDescription = function () {
        // utilisation de litteraux de gabarits ou string template, sucre synthaxique fourni par Typescript
        return "Ce media est un " + this.type + ". Son id est " + this.id + ". Son titre est " + this.titre + ". Il reste " + this.nbExemplaires + " exemplaire(s).";
    };
    /**
     * retourne le nb d'exemplaire disponible pour ce média
     *
     * @returns {number}
     * @memberof Media
     */
    Media.prototype.getNbExemplairesDispo = function () {
        return this.nbExemplaires;
    };
    /**
     * Méthode faisant appel à la méthode getDateDispoMedia de la mediateque pour connaitre la prochaine date de disponibilité dudit média
     *
     * @returns {Date}
     * @memberof Media
     */
    Media.prototype.getDateDispo = function () {
        // on déleque cette tâche a la médiateque car c'est elle qui gère la collection des emprunts indiquant les dates de retour des medias 
        return acteurs_1.Mediatheque.getInstance().getDateDispoMedia(this);
    };
    /**
     * Ajoute un exemplaire au média en cours
     *
     * @memberof Media
     */
    Media.prototype.ajoutExemplaire = function () {
        console.log("Le nombre d'exemplaire de m\u00E9dia " + this.id + " est pass\u00E9 de " + this.nbExemplaires + " \u00E0 " + (this.nbExemplaires += 1));
    };
    /**
     * Retire un exemplaire ua media en cours
     *
     * @memberof Media
     */
    Media.prototype.retirerExemplaire = function () {
        console.log("Le nombre d'exemplaire de m\u00E9dia " + this.id + " est pass\u00E9 de " + this.nbExemplaires + " \u00E0 " + (this.nbExemplaires -= 1));
    };
    return Media;
}());
exports.Media = Media;
/*
* Calsse fille héritée de Media permettant d'instancier un Livre.
*
* @export
* @class Livre
* @extends {Media}
*/
var Livre = /** @class */ (function (_super) {
    __extends(Livre, _super);
    /**
     * Creer nune instance de Livre hérité de Media
     * @param {number} [_idEnregistre=0] - Si ce parametre est connu (cas de chargement d'un media existant enregistré depuis un fichier texte) alotrs on le fournit ici, sinon on donne la valeur 0 s'il s'agit de la création d'un nouveau média
     * @param {string} _isbn - L'International Standard Book Number (ISBN) ou Numéro international normalisé du livre est un numéro internationalement reconnu, créé en 1970, identifiant de manière unique chaque édition de chaque livre publié
     * @memberof Livre
     */
    function Livre(_idEnregistre, _isbn, _titre, _nbExemplaires, _auteur, _genre, _maisonEdition, _nbPage) {
        if (_idEnregistre === void 0) { _idEnregistre = 0; }
        var _this = 
        // appel du constructeur de Media pour le constructeur des attributs hérités
        _super.call(this, _idEnregistre, "livre", _titre, _nbExemplaires, _isbn) || this;
        _this.auteur = _auteur;
        _this.genre = _genre;
        //todo tester que le genre est présent dans le genresArray
        _this.maisonEdition = _maisonEdition;
        _this.nbPage = _nbPage;
        _this.ISBN = _isbn;
        return _this;
    }
    /**
     * Retourne la description d'un livre
     * @return {string} La maison d'edition, l'auteur, le genre, l'ISBN et le nombre de pages.
     */
    Livre.prototype.getDescription = function () {
        return _super.prototype.getDescription.call(this) + " Sa maison d'\u00E9dition est " + this.maisonEdition + ". Son auteur est " + this.auteur + ". Son genre est " + this.genre + ". Son ISBN est " + this.ISBN + ". Il poss\u00E8de " + this.nbPage + " page(s)";
    };
    Livre.genresLivre = ["Policer", "Essai", "SF", "Conte", "Roman", "Biographie"];
    return Livre;
}(Media));
exports.Livre = Livre;
/**
 * Classe fille de la classe mère "media" permettant d'instancier un CD.
 * @attribut {number} discID : Le DiscId est un identifiant calculable pour tout CD Audio. Cet identifiant est un nombre dont la valeur dépend du nombre de pistes sur le CD, et de la position de chaque piste. Unique pour chaque édition de cd
 */
var CD = /** @class */ (function (_super) {
    __extends(CD, _super);
    function CD(_idEnregistre, _discID, _titre, _nbExemplaires, _groupe, _genre, _producteur, _dureeMn) {
        if (_idEnregistre === void 0) { _idEnregistre = 0; }
        var _this = _super.call(this, _idEnregistre, "CD", _titre, _nbExemplaires, _discID) || this;
        _this.groupe = _groupe;
        _this.genre = _genre;
        _this.producteur = _producteur;
        _this.dureeMn = _dureeMn;
        _this.discID = _discID;
        return _this;
    }
    // Retourne la description d'un CD avec le nom du producteur, son discID, son genre, son groupe et sa durée.
    CD.prototype.getDescription = function () {
        return _super.prototype.getDescription.call(this) + " Son producteur est " + this.producteur + ". Son discID est " + this.discID + ". Son genre est " + this.genre + ". Son groupe est " + this.groupe + ". Sa dur\u00E9e est " + this.dureeMn + " mn.";
    };
    CD.genresCD = ["Pop", "Rock", "Classique", "Jazz", "Rap"];
    return CD;
}(Media));
exports.CD = CD;
/**
 * Classe fille de la classe mère "media" permettant d'instancier un DVD.
 * @attribut {number} EIRD : Identifiant unique d'un DVD
 */
var DVD = /** @class */ (function (_super) {
    __extends(DVD, _super);
    function DVD(_idEnregistre, _EIRD, _titre, _nbExemplaires, _realisateur, _genre, _producteur, _dureeMn) {
        if (_idEnregistre === void 0) { _idEnregistre = 0; }
        var _this = _super.call(this, _idEnregistre, "DVD", _titre, _nbExemplaires, _EIRD) || this;
        _this.realisateur = _realisateur;
        _this.genre = _genre;
        _this.producteur = _producteur;
        _this.dureeMn = _dureeMn;
        _this.EIRD = _EIRD;
        return _this;
    }
    //Retourne la description d'un DVD avec son EIRD, son réalisateur, son genre, son producteur et sa durée.
    DVD.prototype.getDescription = function () {
        return _super.prototype.getDescription.call(this) + " Son EIRD est " + this.EIRD + ". Son r\u00E9alisateur est " + this.realisateur + ". Son genre est " + this.genre + ". Son producteur est " + this.producteur + ". Sa dur\u00E9e est de " + this.dureeMn + " mn.";
    };
    DVD.genresDVD = ["Comédie", "Drame", "Thriller", "Action", "SF"];
    return DVD;
}(Media));
exports.DVD = DVD;
/**
 * Classe mère permettant d'instancier un emprunt.
 * @attribut {number} id : attribut généré via la fonction generateUniqueId.
 * @attribut {number} idMedia : identifiant du média que ce soit un livre un cd ou un dvd.
 * @attribut {number} idEmprunteur : identiifant de l'emprunteur.
 * @attribut {Date} dateEmprunt : date d'emprunt du média
 * @attribut {Date} dateRetour : date de retour prévu de l'emprunt (j+7 de la date d'emprunt)
 * @attribut {string} indexPourRecherche : l'index de recherche pourrait également contenir la date d'emprunt mais dans la mesure ou un media ne peut être emprunter qu'une seule fois par un emprunteur l'id du media et de l'emprunteur suffisent
*/
var Emprunt = /** @class */ (function () {
    /* les dates servent uniquement pour le chargement des emprunts enregistrées mais pas lors de la création d'un nouvel emprunt qui utilise la date du jour dans le constructor */
    function Emprunt(_idRecupere, _idMedia, _idEmprunteur, _dateEmprunt, _dateRetour) {
        if (_idRecupere === void 0) { _idRecupere = 0; }
        if (_dateEmprunt === void 0) { _dateEmprunt = null; }
        if (_dateRetour === void 0) { _dateRetour = null; }
        var dateManager = utils_2.SystemDateManager.instance;
        this.id = _idRecupere ? _idRecupere : utils_1.generateUniqueId(); //media.id + emprunteur.id + dateManager.formatDateToString('jj/mm/aaaa');
        this.dateEmprunt = (_dateEmprunt) ? _dateEmprunt : dateManager.date;
        this.dateRetour = (_dateRetour) ? _dateRetour : dateManager.addDays(7).date;
        this.idMedia = _idMedia;
        this.idEmprunteur = _idEmprunteur;
        this.indexPourRecherche = (_idMedia + _idEmprunteur).toString();
    }
    //Retourne une description complète de l'emprunt avec : le media qui est concerné, sa date d'emprunt et de retour, l'auteur de l'emprunt.
    Emprunt.prototype.getDescription = function () {
        var mediaEmprunte = acteurs_1.Mediatheque.getInstance().getMedia(this.idMedia);
        var emprunteur = acteurs_1.Mediatheque.getInstance().getEmprunteur(this.idEmprunteur);
        /* ajout d'une méthode formatDateToString à SystemDateManager pour retourner dans la description au format string */
        return "Description de l'emprunt :\n        Media concern\u00E9 : " + mediaEmprunte.getDescription() + " \n        Il a \u00E9t\u00E9 emprunt\u00E9 le " + utils_2.SystemDateManager.instance.changeDate(this.dateEmprunt).formatDateToString("jj/mm/aaaa") + " \n            par : " + emprunteur.getDescription() + " \n        et sera a retourner avant le " + utils_2.SystemDateManager.instance.changeDate(this.dateRetour).formatDateToString("jj/mm/aaaa à hh:mn:ss");
    };
    return Emprunt;
}());
exports.Emprunt = Emprunt;
//# sourceMappingURL=medias.js.map