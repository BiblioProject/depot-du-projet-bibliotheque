"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var acteurs_1 = require("./acteurs");
var medias_1 = require("./medias");
/**
 * Classe mère permettant de définir les attributs de base d'un Media pour la conversion d'un objet en instance de Media
 * permet de bénéficier de l'auto completion sur les données récuperer au format json et d'alleger la méthode restaurerDonnees de Mediatheque
 *
 * @export
 * @abstract - Cette classe mere abstraite ne peut être instanciée directement
 * @class MediaConverter
 */
var MediaConverter = /** @class */ (function () {
    function MediaConverter(_objectAConvertir) {
        this.id = _objectAConvertir.id;
        this.type = _objectAConvertir.type;
        this.titre = _objectAConvertir.titre;
        this.nbExemplaires = _objectAConvertir.nbExemplaires;
        this.indexPourRecherche = _objectAConvertir.indexPourRecherche;
    }
    return MediaConverter;
}());
exports.MediaConverter = MediaConverter;
/**
 * Permet de convertir un objet json en instance de classe Livre
 *
 * @export
 * @class LivreConverteur
 * @extends {MediaConverter}
 */
var LivreConverteur = /** @class */ (function (_super) {
    __extends(LivreConverteur, _super);
    function LivreConverteur(_objectAConvertir) {
        var _this = _super.call(this, _objectAConvertir) || this;
        _this.ISBN = _objectAConvertir.ISBN;
        _this.auteur = _objectAConvertir.auteur;
        _this.genre = _objectAConvertir.genre;
        _this.maisonEdition = _objectAConvertir.maisonEdition;
        _this.nbPage = _objectAConvertir.nbPage;
        return _this;
    }
    LivreConverteur.prototype.conversionClasse = function () {
        return new medias_1.Livre(this.id, this.ISBN, this.titre, this.nbExemplaires, this.auteur, this.genre, this.maisonEdition, this.nbPage);
    };
    return LivreConverteur;
}(MediaConverter));
exports.LivreConverteur = LivreConverteur;
/**
 * Permet de convertir un objet json en instance de classe CD
 *
 * @export
 * @class CDConverteur
 * @extends {MediaConverter}
 */
var CDConverteur = /** @class */ (function (_super) {
    __extends(CDConverteur, _super);
    function CDConverteur(_objectAConvertir) {
        var _this = _super.call(this, _objectAConvertir) || this;
        _this.discID = _objectAConvertir.discID;
        _this.groupe = _objectAConvertir.groupe;
        _this.genre = _objectAConvertir.genre;
        _this.producteur = _objectAConvertir.producteur;
        _this.dureeMn = _objectAConvertir.dureeMn;
        return _this;
    }
    CDConverteur.prototype.conversionClasse = function () {
        return new medias_1.CD(this.id, this.discID, this.titre, this.nbExemplaires, this.groupe, this.genre, this.producteur, this.dureeMn);
    };
    return CDConverteur;
}(MediaConverter));
exports.CDConverteur = CDConverteur;
/**
 * Permet de convertir un objet json en instance de classe DVD
 *
 * @export
 * @class DVDConverteur
 * @extends {MediaConverter}
 */
var DVDConverteur = /** @class */ (function (_super) {
    __extends(DVDConverteur, _super);
    function DVDConverteur(_objectAConvertir) {
        var _this = _super.call(this, _objectAConvertir) || this;
        _this.EIRD = _objectAConvertir.EIRD;
        _this.realisateur = _objectAConvertir.realisateur;
        _this.genre = _objectAConvertir.genre;
        _this.producteur = _objectAConvertir.producteur;
        _this.dureeMn = _objectAConvertir.dureeMn;
        return _this;
    }
    DVDConverteur.prototype.conversionClasse = function () {
        return new medias_1.DVD(this.id, this.EIRD, this.titre, this.nbExemplaires, this.realisateur, this.genre, this.producteur, this.dureeMn);
    };
    return DVDConverteur;
}(MediaConverter));
exports.DVDConverteur = DVDConverteur;
/**
 * Permet de convertir un objet json en instance de classe Emprunteur
 *
 * @export
 * @class EmprunteurConverteur
 */
var EmprunteurConverteur = /** @class */ (function () {
    function EmprunteurConverteur(_objectAConvertir) {
        this.id = _objectAConvertir.id;
        this.nom = _objectAConvertir.nom;
        this.prenom = _objectAConvertir.prenom;
        this.email = _objectAConvertir.email;
    }
    EmprunteurConverteur.prototype.conversionClasse = function () {
        return new acteurs_1.Emprunteur(this.id, this.nom, this.prenom, this.email);
    };
    return EmprunteurConverteur;
}());
exports.EmprunteurConverteur = EmprunteurConverteur;
/**
 * Permet de convertir un objet json en instance de classe Emprunt
 *
 * @export
 * @class EmpruntConverteur
 */
var EmpruntConverteur = /** @class */ (function () {
    function EmpruntConverteur(_objectAConvertir) {
        this.id = _objectAConvertir.id;
        this.idMedia = _objectAConvertir.idMedia;
        this.idEmprunteur = _objectAConvertir.idEmprunteur;
        this.dateEmprunt = new Date(_objectAConvertir.dateEmprunt);
        this.dateRetour = new Date(_objectAConvertir.dateRetour);
    }
    EmpruntConverteur.prototype.conversionClasse = function () {
        return new medias_1.Emprunt(this.id, this.idMedia, this.idEmprunteur, this.dateEmprunt, this.dateRetour);
    };
    return EmpruntConverteur;
}());
exports.EmpruntConverteur = EmpruntConverteur;
//# sourceMappingURL=objectToClassConverteur.js.map