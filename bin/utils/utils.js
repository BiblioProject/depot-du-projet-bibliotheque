"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Fonction permettant de générer un identifiant unique
 *
 * @export
 * @returns {number}
 */
function generateUniqueId() {
    var uid1 = Date.now();
    var uid2;
    while ((uid2 = Date.now()) == uid1)
        ;
    return uid2;
}
exports.generateUniqueId = generateUniqueId;
/**
 * Fonction permettant d'envoyer un mail
 *
 * @export
 * @param {string}  address - adresse de destinataire de l'email
 * @param {string}  subject - sujet de l'email
 * @param {string}  content - contenu textuel de l'email
 */
function sendMail(address, subject, content) {
    var mail = "Mail from: mediatheque@lecnam.net\n    To: " + address + "\n    Subject: " + subject + "\n    \n    " + content;
    console.log(mail);
}
exports.sendMail = sendMail;
/**
 * Classe permettant des opérations récurrentes sur les dates
 *
 * @export
 * @class SystemDateManager
 */
var SystemDateManager = /** @class */ (function () {
    /**
     *Creates an instance of SystemDateManager.
     * @memberof SystemDateManager
     */
    function SystemDateManager() {
        this._date = new Date();
    }
    /**
     * Méthode publique ajoutée permettant de remplacer la date instanciée dans le constructeur par une autre date
     *
     * @param {Date} date
     * @returns {SystemDateManager}
     * @memberof SystemDateManager
     */
    SystemDateManager.prototype.changeDate = function (date) {
        this._date = date;
        return this;
    };
    Object.defineProperty(SystemDateManager.prototype, "date", {
        /**
         * Retourne la date de SystemDateManager
         *
         * @readonly
         * @type {Date}
         * @memberof SystemDateManager
         */
        get: function () {
            return new Date(this._date);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Méthode permettant de formatter une date en string selon différents formats
     * pour un plus gros projet il serait possible d'utiliser une bibliothèque externe type moment.js pour faciliter la gestion des dates
     *
     * @param {string}  typeFormat - Format à utiliser pour la date retournée. Valeurs possibles : "time" / "jj/mm/aaaa"
     * @param {boolean=} afficherHeure - Ajouter l'heure au format hh/mm/ss
     * @return {string} Date retournée au format string selon typeFormat et afficherHeure
     * @memberof SystemDateManager
     */
    SystemDateManager.prototype.formatDateToString = function (typeFormat, afficherHeure) {
        if (typeFormat === void 0) { typeFormat = ""; }
        if (afficherHeure === void 0) { afficherHeure = false; }
        // gerer cas ou typeFormat inexistant, format par défaut
        var returnValue = "";
        var optionsToLocaleDateString;
        afficherHeure ? optionsToLocaleDateString = { hour: '2-digit', minute: '2-digit', second: "2-digit" } : optionsToLocaleDateString = {};
        switch (typeFormat) {
            case "time":
                returnValue = (this._date.getTime()).toString();
                break;
            case "jj/mm/aaaa":
                // ES6 standard methode
                Object.assign(optionsToLocaleDateString, { year: 'numeric', month: '2-digit', day: '2-digit' });
                returnValue = this._date.toLocaleDateString("fr-FR", optionsToLocaleDateString);
                break;
        }
        return returnValue;
    };
    /**
     * Permet d'ajouter un nombre de jours à l'objet date de SystemDateManager
     *
     * @param {number} days - Nombre de jours à ajouter
     * @returns {SystemDateManager}
     * @memberof SystemDateManager
     */
    SystemDateManager.prototype.addDays = function (days) {
        this._date = new Date(this._date.getTime() + days * 24 * 60 * 60 * 1000);
        return this;
    };
    /**
     * Retourne la différence en nombre de jour entre la date de SystemDateManager et une autre date passée en paramètre
     *
     * @param {Date} date - date a comparer avec la date de SystemDateManager
     * @returns {number}
     * @memberof SystemDateManager
     */
    SystemDateManager.prototype.diffInDaysWith = function (date) {
        return Math.ceil((this._date.getTime() - date.getTime()) / (24 * 60 * 60 * 1000));
    };
    SystemDateManager.instance = new SystemDateManager();
    return SystemDateManager;
}());
exports.SystemDateManager = SystemDateManager;
//# sourceMappingURL=utils.js.map