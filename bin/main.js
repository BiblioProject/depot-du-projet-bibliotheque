"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var acteurs_1 = require("./classes/acteurs");
var medias_1 = require("./classes/medias");
/**
 * Observe le chargement des données de l'application et lance la suite d'instruction principale du programme.
 * finished() est déclenché lorsque les médias, les emprunteurs et les emprunts sont chargés (chargement de fichier Json)
 * Ces 3 fichiers sont chargés dans ce ordre l'un à la suite des autres par la méthode chargementDonneesApplication() de la classe Mediateque
 * Les instances d'emprunts pour être recontruits nécessitent préalablement que les médias et les emprunteurs le soient également (raison pour laquelle les chargements sont lancés l'un après l'autre)
 * @implements ChargementDonneesObserver
 */
var ObserverChargementDonnees = /** @class */ (function () {
    function ObserverChargementDonnees() {
    }
    ObserverChargementDonnees.prototype.finished = function () {
        console.log("Chargement des donn\u00E9es termin\u00E9");
        // récuperation du singleton Mediateque
        var mediatheque = acteurs_1.Mediatheque.getInstance();
        // création d'emprunteurs en mémoire
        //todo : s'assurer qu'il est impossible d'ajouter 2 email identique
        var emprunteur1 = new acteurs_1.Emprunteur(0, "Bruckler", "Benjamin", "benjamin.bruckler.auditeur@lecnam.net");
        var emprunteur2 = new acteurs_1.Emprunteur(0, "Vincent", "Brice", "brice.vincent21@gmail.com");
        var emprunteur3 = new acteurs_1.Emprunteur(0, "Vincente", "Brice", "brice.vincent21@gmail.come");
        // ajout des emprunteurs précédemment créés si leur email n'est pas déjà présent dans collectionEmprunteur de Mediateque
        // au premier chargement de l'application (lorsque les fichiers de données sont vides) les 3 emprunteurs sont ajoutés à collectionEmprunteur
        // lors des executions suivantes de l'application ils ne le sont plus puisqu'ils ont déjà été enregistré dans la source de données via la méthode sauvegarderDonneesApplication de Mediateque
        mediatheque.ajoutEmprunteur(emprunteur1);
        mediatheque.ajoutEmprunteur(emprunteur2);
        mediatheque.ajoutEmprunteur(emprunteur3);
        mediatheque.ajoutEmprunteur(emprunteur3); // ajout impossible car emprunteur3 a déjà été ajouté
        // création de 3 médias en mémoire
        var livre1 = new medias_1.Livre(0, '12453526191', "Dracula", 1500, "Bram Stoker", "Roman", "Le livre de poche", 1);
        var livre2 = new medias_1.Livre(0, '14164146164', 'titre1', 105, 'auteur', 'genreeee', 'maisonEdit', 200);
        //todo : vérifier qu'il n'est pas possible d'ajouter 2 médias avec le même indexPourRecherche
        var dvd1 = new medias_1.DVD(0, 'B004JP8ONA', "Le seigneur des anneaux, la communauté de l'anneau", 10, "Peter Jackson", "Action", "Metropolitan Vidéo", 171);
        var dvd2 = new medias_1.DVD(0, 'B004JP8OEM', "Le seigneur des anneaux, la communauté de l'anneau (version longue)", 10, "Peter Jackson", "Action", "Metropolitan Vidéo", 171);
        var dvd3 = new medias_1.DVD(0, 'B004JP8OEB', "Le seigneur des anneaux, la communauté de l'anneau (version longue + bonus)", 10, "Peter Jackson", "Action", "Metropolitan Vidéo", 171);
        var cd1 = new medias_1.CD(0, 'B70F500C', 'Lights', 105, 'Archive', 'Rock', 'Archive', 117);
        // ajout des médias créés à collectionMedia de Mediateque
        mediatheque.ajoutMedia(livre1);
        mediatheque.ajoutMedia(livre2);
        mediatheque.ajoutMedia(dvd1);
        mediatheque.ajoutMedia(cd1);
        mediatheque.ajoutMedia(cd1);
        mediatheque.ajoutMedia(dvd2);
        mediatheque.ajoutMedia(dvd3);
        // suppression du media dvd3
        mediatheque.suppressionMedia(dvd3);
        // ajout d'un exemplaire pour le media cd1 si cd1 est bien dans la collection (il peut avoir été supprimé entre temps et n'être disponible qu'en mémoire)
        if (mediatheque.getMedia(cd1.id))
            cd1.ajoutExemplaire();
        if (mediatheque.getMedia(livre2.id))
            livre2.retirerExemplaire();
        // récuperation de médias enregistrés dans collectionMedia (car les variables livre1, livre2, ... ne sont plus forcément dans la collection)
        var mediaEnregistre = mediatheque.getMedia('12453526191');
        var mediaEnregistre1 = mediatheque.getMedia('B004JP8ONM');
        var mediaEnregistre2 = mediatheque.getMedia('14164146164');
        var mediaEnregistre3 = mediatheque.getMedia('B70F500C');
        // récuperation des emprunteurs enregistrés dans collectionemprunteur
        var emprunteurEnregistre = mediatheque.getEmprunteur('benjamin.bruckler.auditeur@lecnam.net');
        var emprunteurEnregistre1 = mediatheque.getEmprunteur('brice.vincent21@gmail.com');
        var emprunteurEnregistre2 = mediatheque.getEmprunteur('brice.vincent21@gmail.come');
        // suppression de l'emprunteur emprunteurEnregistre2
        mediatheque.suppressionEmprunteur(emprunteurEnregistre2);
        // pour ajouter un emprunt il faut déjà vérifié qu'il est disponible et que l'emprunteur peut l'emprunter, c'est ce que fait cette fonction demandeAjoutEmprunt
        mediatheque.demandeAjoutEmprunt(mediaEnregistre, emprunteurEnregistre); // mediaEnregistre qui ne possedait qu'un exemplaire disponible ne sera ensuite plus dispo
        mediatheque.demandeAjoutEmprunt(mediaEnregistre, emprunteurEnregistre); // emprunt impossible car cet utilisateur l'a déjà emprunté
        mediatheque.demandeAjoutEmprunt(mediaEnregistre, emprunteurEnregistre1); // emprunt impossible car le nombre d'exemplaire du media est à 0
        mediatheque.demandeAjoutEmprunt(mediaEnregistre1, emprunteurEnregistre);
        mediatheque.demandeAjoutEmprunt(mediaEnregistre2, emprunteurEnregistre);
        mediatheque.demandeAjoutEmprunt(mediaEnregistre3, emprunteurEnregistre); // emprunt impossible car l'emprunteur a déjà 3 emprunts*/
        //suavegarde des collections (medias, emprunteurs et emprunts)
        mediatheque.sauvegarderDonneesApplication();
    };
    return ObserverChargementDonnees;
}());
// ici, on demande au singleton Mediatheque d'observer le moment ou les donnees de l'application seront toutes chargéés
acteurs_1.Mediatheque.getInstance().registerObserver(new ObserverChargementDonnees());
// et ensuite on lance le chargement de l'application
acteurs_1.Mediatheque.getInstance().chargementDonneesApplication();
//# sourceMappingURL=main.js.map