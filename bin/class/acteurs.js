"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var classConverteur_1 = require("./classConverteur");
var medias_1 = require("./medias");
//todo : créer import pour interfaces
var utils_1 = require("./utils");
var classConverteur_2 = require("./classConverteur");
//comment
var Mediatheque = /** @class */ (function () {
    function Mediatheque() {
        // old comment : utilisation de Map incompatible avec Json stringify qui n'accepte que des index de type string alors que Map peut avoir un index de différents types.
        // l'utilisation d'un Array<typé> pose également problème puisque les index de type string ne permettent pas d'utiliser la propriété lenght
        // et ne permet pas d'acceder directement à un élément. Si la mediathèque venait à comporter des millions de medias et d'emprunteur; le parcours de l'ensemble des collections pour chaque opérations rendrait rapidement l'application inutilisable
        // c'est la raison pour laquelle nous avons choisi l'utilisation d'objet
        //todo : tester quand meme le test de conversion en array<typé> et voir si possible de parcours les index comme un objet
        //voir en meme temps si l'utilisation des méthodes map et filter peuvent être appliquer à un tableau d'objet (il me semble avoir essayé et que ça ne matrchait pas mais restester pour éventuellement montrer au prof les limites que nous avons rencontrés.)
        this.collectionMedia = new Array();
        this.collectionEmprunteur = new Array();
        this.collectionEmprunt = new Array();
        this.observerDonneesChargees = null;
        //voir pour utilisation constantes
        this.fileMedias = 'data/medias.txt';
        this.fileEmprunteurs = 'data/emprunteurs.txt';
        this.fileEmprunts = 'data/emprunts.txt';
    }
    /*Implémentation du singleton Mediatheque*/
    Mediatheque.getInstance = function () {
        return this._instance || (this._instance = new this());
    };
    Mediatheque.prototype.registerObserver = function (new_observer) {
        this.observerDonneesChargees = new_observer;
    };
    /* on charge dejà les medias, ensuite les emprunteurs et ensuite seulement les emprunt (les emprunts nécessite que les données des medias et emprunteurs soient chargés)
    il aurait également été possible d'utiliser des promise et la méthode promise.all pour controler le chargement des 3 fichiers en processus parrallèle mais l'utilisation d'un design pattern observeur a été testé dans ce projet
    */
    Mediatheque.prototype.notifyObserver = function (fichierCharge) {
        if (this.observerDonneesChargees) {
            if (fichierCharge === this.fileMedias)
                this.restaurerDonnees(this.fileEmprunteurs);
            if (fichierCharge === this.fileEmprunteurs)
                this.restaurerDonnees(this.fileEmprunts);
            if (fichierCharge === this.fileEmprunts)
                this.observerDonneesChargees.finished();
        }
    };
    Mediatheque.prototype.chargementDonneesApplication = function () {
        // une fois le chargement des medias effectué l'obseveur sera notifié avec le nom du fichier chargé, il chargement ensuite les emprunteurs puis les emprunts et alors il lancera la méthode finished de l'observeur pour lancer la séquence principale de l'application une fois les données toutes chargées
        this.restaurerDonnees(this.fileMedias);
    };
    Mediatheque.prototype.restaurerDonnees = function (fichier) {
        var _this = this;
        var fs = require('fs');
        // utilisation de promise à cet endroit à venir pour chargement multi thread
        fs.readFile(fichier, function (err, donnees) {
            if (err)
                throw err;
            if (donnees.length != 0) {
                var collectionObject = JSON.parse(donnees);
                var mediateque = Mediatheque.getInstance();
                for (var _i = 0, collectionObject_1 = collectionObject; _i < collectionObject_1.length; _i++) {
                    var objectJson = collectionObject_1[_i];
                    if (fichier === _this.fileMedias) {
                        // pour le moment la variable objet n'a pas de type on peut donc chercher la méthode type dessus (solution alternative en recherche)
                        switch (objectJson.type) {
                            case "livre":
                                _this.ajoutMedia(new classConverteur_1.LivreConverteur(objectJson).conversionClasse());
                                break;
                            case "CD":
                                _this.ajoutMedia(new classConverteur_1.CDConverteur(objectJson).conversionClasse());
                                break;
                            case "DVD":
                                _this.ajoutMedia(new classConverteur_1.DVDConverteur(objectJson).conversionClasse());
                                break;
                        }
                    }
                    else if (fichier === _this.fileEmprunteurs) {
                        _this.ajoutEmprunteur(new classConverteur_2.EmprunteurConverteur(objectJson).conversionClasse());
                    }
                    else if (fichier === _this.fileEmprunts) {
                        // ajout via méthode ajoutEmprunt qui ajoute également l'emprunt à la collection de l'emprunteur
                        _this.ajoutEmprunt(new classConverteur_1.EmpruntConverteur(objectJson).conversionClasse());
                    }
                }
            }
            Mediatheque.getInstance().notifyObserver(fichier);
        });
    };
    //comment
    Mediatheque.prototype.sauvegarderCollections = function () {
        this.sauvegarderCollection(this.collectionMedia, this.fileMedias);
        this.sauvegarderCollection(this.collectionEmprunteur, this.fileEmprunteurs);
        this.sauvegarderCollection(this.collectionEmprunt, this.fileEmprunts);
    };
    Mediatheque.prototype.sauvegarderCollection = function (collection, fichier) {
        var fs = require('fs');
        //formater element en json + écriture dans le fichier passé en paramètre
        var elementsJson = JSON.stringify(collection);
        fs.writeFileSync(fichier, elementsJson);
    };
    //todo fusionner ajoutEmprunteur et ajoutMedia
    //comment
    //comment
    Mediatheque.prototype.ajoutCollection = function (collection, ajout) {
        collection.push(ajout);
        // old comment ancienne solution : 
        /*if (!collection[ajout.id]) collection[ajout.id] = ajout;
        else console.log(`L'élément ${ajout.constructor.name} "${ajout.id}" est déjà enregistré dans la médiathèque`);
        */
        // si l'emprunteur n'est pas déjà dans la collection, on peut l'ajouter
        // on extrait uniquement les instances du type T de la collection
        // collectionT : Array<Media> = this.collectionMedia.filter(media => media.constructor.name == ajout.constructor.name);
        //let presentInCollection : Array<T> = collectionT.filter(media => media.constructor.name == type);
        // nous avons utilisé cette solution et non la méthode push comme conseillée pour ne pas avoir à reparcourir l'ensemble des éléments de la collection pour effectuer une opération sur un objet en particulier. 
        // l'idée était d'acceleler la recherche d'un élément en le ciblant directement avec son identifiant
        // si c'était à refaire nous aurions utilisé des tableaux à la place et un parcours via iterateur ou iterateur implicite de chacun des éléments pour les opérations.
        // cette solution a le principal inconvénient de ne pas permettre d'utiliser les méthodes filter / map native des Arrays et par conséquent ne facilite pas la lecture du code.
        // bien que l'accès à un élément soit facilité ainsi que leur suppression nous avons bien conscience que cette solution n'est pas idéale mais ce projet à permis d'explorer différentes pistes
    };
    Mediatheque.prototype.suppressionCollection = function (collection, retrait) {
        for (var i = collection.length - 1; i >= 0; i--) {
            var element = collection[i];
            if (element.id == retrait.id) {
                this.collectionMedia.splice(i, 1);
                //.constructor.name permet d'avoir le nom de classe de l'instance à l'execution du programme
                console.log("L'" + retrait.constructor.name + " " + retrait.id + " a \u00E9t\u00E9 supprim\u00E9 de la m\u00E9diath\u00E8que");
                break; //sortie de la boucle une fois l'élément supprimé pour éviter un parcours inutile (uniquement un seul indexIdentique ne peut exister dans la collection)
            }
        }
    };
    Mediatheque.prototype.ajoutMedia = function (ajout) {
        var findedIndex = this.collectionMedia.filter(function (media) { return media.id == ajout.id; });
        if (findedIndex.length > 0)
            console.log("Ajout impossible, le m\u00E9dia " + ajout.id + " existe d\u00E9j\u00E0 dans la m\u00E9diath\u00E8que");
        else
            this.collectionMedia.push(ajout);
    };
    //comment
    Mediatheque.prototype.suppressionMedia = function (retrait) {
        this.suppressionCollection(this.collectionMedia, retrait);
    };
    Mediatheque.prototype.ajoutEmprunteur = function (ajout) {
        var findedIndex = this.collectionEmprunteur.filter(function (emprunteur) { return emprunteur.id == ajout.id; });
        if (findedIndex.length > 0)
            console.log("Ajout impossible, l'emprunteur " + ajout.id + " existe d\u00E9j\u00E0 dans la m\u00E9diath\u00E8que");
        else
            this.collectionEmprunteur.push(ajout);
    };
    //comment
    Mediatheque.prototype.suppressionEmprunteur = function (retrait) {
        this.suppressionCollection(this.collectionEmprunteur, retrait);
    };
    /* lorsqu'un emprunteur rend un media le nombre d'exemplaire est augmenté */
    Mediatheque.prototype.getDateDispoMedia = function (media) {
        var result = this.collectionEmprunt.filter(function (emprunt) { return emprunt.idMedia === media.id; }).reduce(function (min, val) { return val < min ? val : min; });
        return result ? result.dateRetour : null;
    };
    //comment
    Mediatheque.prototype.demandeAjoutEmprunt = function (media, emprunteur) {
        /*new Emprunt(mediaEnregistre.id,emprunteurEnregistre.id)
        let emprunteur = this.getEmprunteur(emprunt.idEmprunteur);*/
        if (media && emprunteur) {
            var empruntValide = emprunteur.emprunterMedia(media);
            if (empruntValide)
                this.ajoutEmprunt(empruntValide);
        }
        else {
            console.log("Le média ou l'emprunteur utilisé pour la demande d'éjout du média n'est pas valide");
        }
    };
    //comment
    Mediatheque.prototype.ajoutEmprunt = function (emprunt) {
        var emprunteur = this.getEmprunteur(emprunt.idEmprunteur);
        var media = this.getMedia(emprunt.idMedia);
        this.collectionEmprunt.push(emprunt);
        this.majCollectionEmpruntEmprunteur(emprunteur);
        media.retirerExemplaire();
        console.log("L'emprunteur " + emprunt.idEmprunteur + " vient d'emprunter le media " + emprunt.idMedia);
    };
    Mediatheque.prototype.retourEmprunt = function (emprunt) {
        var emprunteur = this.getEmprunteur(emprunt.idEmprunteur);
        var media = this.getMedia(emprunt.idMedia);
        this.suppressionCollection(this.collectionEmprunt, emprunt);
        this.majCollectionEmpruntEmprunteur(emprunteur);
        media.ajoutExemplaire();
        console.log("L'emprunteur " + emprunt.idEmprunteur + " vient de retourner le media " + emprunt.idMedia);
    };
    Mediatheque.prototype.majCollectionEmpruntEmprunteur = function (emprunteur) {
        emprunteur.updateCollectionEmprunt(this.getEmpruntByEmprunteur(emprunteur));
    };
    //comment
    Mediatheque.prototype.getEmpruntByEmprunteur = function (emprunteur) {
        return this.collectionEmprunt.filter(function (emprunt) { return emprunt.idEmprunteur === emprunteur.id; });
    };
    Mediatheque.prototype.getMedia = function (identificateur) {
        if (typeof identificateur === "string")
            return this.getElementInCollectionByIndexrecherche(this.collectionMedia, identificateur);
        else
            return this.getElementInCollectionById(this.collectionMedia, identificateur);
    };
    Mediatheque.prototype.getEmprunteur = function (identificateur) {
        return typeof identificateur === "string"
            ? this.getElementInCollectionByIndexrecherche(this.collectionEmprunteur, identificateur)
            : this.getElementInCollectionById(this.collectionEmprunteur, identificateur);
    };
    //comment
    Mediatheque.prototype.getElementInCollectionByIndexrecherche = function (collection, indexRecherche) {
        return collection.find(function (element) { return element.indexPourRecherche === indexRecherche; });
    };
    //comment
    Mediatheque.prototype.getElementInCollectionById = function (collection, id) {
        return collection.find(function (element) { return element.id === id; });
    };
    /* retourne un nouveau tableau de Media avec uniquement les instances d'un type particulier (ex : CD, DVD, livre) */
    Mediatheque.prototype.getTypedMediaInCollection = function (ajout) {
        return this.collectionMedia.filter(function (media) { return media.constructor.name == ajout.constructor.name; });
    };
    //old solution : private suppressionCollection(collection : object, idASupprimer : string) : string {
    /* optimisation, voir si possible d'utiliser la méthode de description du média pour connaitre son type et son identifiant. Peut être en implémentant une interface descriptible sur la collection  */
    /*if (collection[idASupprimer]) {
        delete collection[idASupprimer];
        return "Elémént supprimé";
    } else {
        return "Elémént non supprimé. L'identifiant de l'élément n'existe pas";
    }*/
    //}
    //todo finir cette fonction (ancienne version)
    Mediatheque.prototype.envoyerEmailEmprunteurEnRetard = function () {
        var dateManager = utils_1.SystemDateManager.instance;
        for (var i = this.collectionEmprunt.length - 1; i >= 0; i--) {
            var emprunt = this.collectionEmprunt[i];
            var emprunteur = this.getEmprunteur(emprunt.idEmprunteur);
            var media = this.getMedia(emprunt.idMedia);
            if (emprunt.dateRetour < dateManager.date)
                utils_1.sendMail(emprunteur.email, "Retard mediatheque de " + dateManager.diffInDaysWith(emprunt.dateRetour) + " jour(s)", "Merci de rapporter le media suivant : " + media.getDescription());
        }
    };
    /*
    todo : pas utilisé pour le moment mais ça va venir
    Voir si on laisse les tableau de genre là ou ailleurs, l'idée est de les rendre accessible à la fois aux classes qui héritent de Media
    et à l'UI qui fournirait à l'utilisateur l'ensemble des valeurs de saisie possible à l'utilisateur
    (une liste déroulante en sortant du cadre du projet), il y aura peut être une classe dédiée pour les différnetes sources textuelles
    */
    Mediatheque.genresLivre = ["Policer", "Essai", "SF", "Conte", "Roman", "Biographie"]; // pas à la bonne place (test)
    Mediatheque.genresCD = ["Pop", "Rock", "Classique", "Jazz", "Rap"]; // pas à la bonne place (test)
    Mediatheque.genresDVD = ["Comédie", "Drame", "Thriller", "Action", "SF"]; // pas à la bonne place (test)
    return Mediatheque;
}());
exports.Mediatheque = Mediatheque;
//comment
var Emprunteur = /** @class */ (function () {
    function Emprunteur(_idEnregistre, nom, prenom, email) {
        if (_idEnregistre === void 0) { _idEnregistre = 0; }
        this.id = _idEnregistre ? _idEnregistre : utils_1.generateUniqueId();
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        //this.collectionMediaEmprunte = _collectionMediaEmprunte;
        this.indexPourRecherche = email;
        this.collectionEmprunt = [];
    }
    /* mise à jour de la liste d'emprunt à chaque ajout d'un emprunt par la bibliotheque
    en récuperant la liste des emprunts d'un utilisateur depuis la collection d'emprunt de la mediatheque
    on s'assure de la cohérence entre les 2 collections. */
    Emprunteur.prototype.updateCollectionEmprunt = function (collection) {
        this.collectionEmprunt = collection;
    };
    //comment
    Emprunteur.prototype.getDescription = function () {
        return "Description de l'emprunteur : \n        - nom : " + this.nom + "\n        - prenom : " + this.prenom + "\n        - email : " + this.email;
    };
    //comment
    Emprunteur.prototype.emprunterMedia = function (media) {
        if (this.peutEmprunterMedia(media))
            return new medias_1.Emprunt(0, media.id, this.id);
        else
            return null;
    };
    //comment
    Emprunteur.prototype.emprunteDejaMedia = function (mediaAEmprunter) {
        var mediatheque = Mediatheque.getInstance();
        var mediaDejaEmprunte = this.collectionEmprunt.find(function (emprunt) { return emprunt.idMedia === mediaAEmprunter.id; });
        return mediaDejaEmprunte === undefined ? false : true;
    };
    //comment
    Emprunteur.prototype.peutEmprunterMedia = function (media) {
        if (this.collectionEmprunt.length < 3) {
            if (!this.emprunteDejaMedia(media)) {
                if (media.getNbExemplairesDispo() > 0)
                    return true;
                else {
                    var dateManager = utils_1.SystemDateManager.instance;
                    var dateRetourFormatte = dateManager.changeDate(media.getDateDispo()).formatDateToString("jj/mm/aaaa");
                    console.log("Le " + media.constructor.name + " \"" + media.id + "\" n'est plus disposible pour l'emprunt. Il sera a nouveau disponbible le " + dateRetourFormatte);
                    return false;
                }
            }
            else {
                console.log("L'emprunteur " + this.id + " doit d\u00E9j\u00E0 rendre le " + media.constructor.name + " \"" + media.id + "\".");
                return false;
            }
        }
        else {
            console.log("L'emprunteur " + this.id + " a atteint le nombre maximum d'emprunt. Il doit rendre un emprunt pour en emprunter un nouveau");
            return false;
        }
    };
    return Emprunteur;
}());
exports.Emprunteur = Emprunteur;
//# sourceMappingURL=acteurs.js.map