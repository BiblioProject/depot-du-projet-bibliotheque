"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var acteurs_1 = require("./acteurs");
var utils_1 = require("./utils");
var utils_2 = require("./utils");
/**
 * Classe mère permettant de définir un media.
 * @attribut {number} id : attribut généré via la fonction generateUniqueId.
 */
var Media = /** @class */ (function () {
    //L'idEnregistré est fourni quand on crée un Media suite au chargement des données de l'application avec les MediaConverteur
    function Media(_idEnregistre, _type, _titre, _nbExemplaires, _indexPourRecherche) {
        if (_idEnregistre === void 0) { _idEnregistre = 0; }
        this.id = _idEnregistre ? _idEnregistre : utils_1.generateUniqueId();
        this.type = _type;
        this.titre = _titre;
        this.nbExemplaires = _nbExemplaires;
        this.indexPourRecherche = _indexPourRecherche;
    }
    /* Retourne la description d'un média, méthode surchargée dans les classes filles Cd, DVD, Livre
     *@return {string} Type de média, son identifiant, son titre et le nombre d'exemplaires.
     */
    Media.prototype.getDescription = function () {
        // utilisation de litteraux de gabarits ou string template, sucre synthaxique fourni par Typescript
        return "Ce media est un " + this.type + ". Son id est " + this.id + ". Son titre est " + this.titre + ". Il reste " + this.nbExemplaires + " exemplaire(s).";
    };
    /* Retourne le nombre d'exemplaire encore disponible pour ce média. */
    Media.prototype.getNbExemplairesDispo = function () {
        return this.nbExemplaires;
    };
    /* Retourne la prochaine date de disponibilité du média (la date à laquelle l'emprunteur rendra le média) */
    /* méthode utilisée uniquement quand le nombre d'exemplaire est égal à 0 */
    Media.prototype.getDateDispo = function () {
        /* todo :
            - si getDispo est différent de 0 dateDispo == now
            - sinon parcours de l'ensemble des emprunts de type T via iterateur et retourne la date de retour la plus proche
            - utilisation de programmation fonctionnelle pour mapper une fonction qui
        */
        return acteurs_1.Mediatheque.getInstance().getDateDispoMedia(this);
    };
    /* il serait possible de n'utiliser qu'une seule méthode changerNbexemplaire mais choix d'une méthode dédiée qui permettrait d'appliquer d'autres traitement par la suite en cas de besoin si l'application venait à évoluer*/
    Media.prototype.ajoutExemplaire = function () {
        console.log("Le nombre d'exemplaire de m\u00E9dia " + this.id + " est pass\u00E9 de " + this.nbExemplaires + " \u00E0 " + (this.nbExemplaires += 1));
    };
    //comment
    Media.prototype.retirerExemplaire = function () {
        console.log("Le nombre d'exemplaire de m\u00E9dia " + this.id + " est pass\u00E9 de " + this.nbExemplaires + " \u00E0 " + (this.nbExemplaires -= 1));
    };
    return Media;
}());
exports.Media = Media;
/**
 * Classe fille de la classe mère "media" permettant de définir un Livre.
 * @attribut {number} ISBN : Identifiant unique d'un livre qui sera saisie par l'utilisateur final de l'application
 */
var Livre = /** @class */ (function (_super) {
    __extends(Livre, _super);
    function Livre(_idEnregistre, _isbn, _titre, _nbExemplaires, _auteur, _genre, _maisonEdition, _nbPage) {
        if (_idEnregistre === void 0) { _idEnregistre = 0; }
        var _this = _super.call(this, _idEnregistre, "livre", _titre, _nbExemplaires, _isbn) || this;
        _this.auteur = _auteur;
        _this.genre = _genre;
        //todo tester que le genre est présent dans le genresArray
        _this.maisonEdition = _maisonEdition;
        _this.nbPage = _nbPage;
        _this.ISBN = _isbn;
        return _this;
    }
    /**
     * Fonction de récupération de la description d'un livre.
     * @return {string} La maison d'edition, l'auteur, le genre, l'ISBN et le nombre de pages.
     */
    //comment : principe de surcharge a rappelé rapidement
    Livre.prototype.getDescription = function () {
        return _super.prototype.getDescription.call(this) + " Sa maison d'\u00E9dition est " + this.maisonEdition + ". Son auteur est " + this.auteur + ". Son genre est " + this.genre + ". Son ISBN est " + this.ISBN + ". Il poss\u00E8de " + this.nbPage + " page(s)";
    };
    return Livre;
}(Media));
exports.Livre = Livre;
/**
 * Classe fille de la classe mère "media" permettant de définir un CD.
 * @attribut {number} discID : Identifiant unique d'un CD qui sera saisi par l'utilisateur final de l'application
 */
var CD = /** @class */ (function (_super) {
    __extends(CD, _super);
    //comment
    function CD(_idEnregistre, _discID, _titre, _nbExemplaires, _groupe, _genre, _producteur, _dureeMn) {
        if (_idEnregistre === void 0) { _idEnregistre = 0; }
        var _this = _super.call(this, _idEnregistre, "CD", _titre, _nbExemplaires, _discID) || this;
        _this.groupe = _groupe;
        _this.genre = _genre;
        _this.producteur = _producteur;
        _this.dureeMn = _dureeMn;
        _this.discID = _discID;
        return _this;
    }
    //Retourne la description d'un CD avec le nom du producteur, son discID, son genre, son groupe et sa durée.
    CD.prototype.getDescription = function () {
        return _super.prototype.getDescription.call(this) + " Son producteur est " + this.producteur + ". Son discID est " + this.discID + ". Son genre est " + this.genre + ". Son groupe est " + this.groupe + ". Sa dur\u00E9e est " + this.dureeMn + " mn.";
    };
    return CD;
}(Media));
exports.CD = CD;
/**
 * Classe fille de la classe mère "media" permettant de définir un DVD.
 * @attribut {number} EIRD : Identifiant unique d'un DVD qui sera saisi par l'utilisateur final de l'application
 */
var DVD = /** @class */ (function (_super) {
    __extends(DVD, _super);
    function DVD(_idEnregistre, _EIRD, _titre, _nbExemplaires, _realisateur, _genre, _producteur, _dureeMn) {
        if (_idEnregistre === void 0) { _idEnregistre = 0; }
        var _this = _super.call(this, _idEnregistre, "DVD", _titre, _nbExemplaires, _EIRD) || this;
        _this.realisateur = _realisateur;
        _this.genre = _genre;
        _this.producteur = _producteur;
        _this.dureeMn = _dureeMn;
        _this.EIRD = _EIRD;
        return _this;
    }
    //Retourne la description d'un DVD avec son EIRD, son réalisateur, son genre, son producteur et sa durée.
    DVD.prototype.getDescription = function () {
        return _super.prototype.getDescription.call(this) + " Son EIRD est " + this.EIRD + ". Son r\u00E9alisateur est " + this.realisateur + ". Son genre est " + this.genre + ". Son producteur est " + this.producteur + ". Sa dur\u00E9e est de " + this.dureeMn + " mn.";
    };
    return DVD;
}(Media));
exports.DVD = DVD;
/**
 * Classe mère permettant de définir un emprunt.
 * @attribut {number} id : attribut généré via la fonction generateUniqueId.
                         idMedia : récupère l'identifiant du média que ce soit un livre un cd ou un dvd.
                        idEmprunteur : récupère l'identiifant de l'emprunteur.
                        
    */
var Emprunt = /** @class */ (function () {
    /* les dates servent uniquement pour le chargement des emprunts enregistrées mais pas lors de la création d'un nouvel emprunt qui utilise la date du jour dans le constructor */
    function Emprunt(_idRecupere, _idMedia, _idEmprunteur, _dateEmprunt, _dateRetour) {
        if (_idRecupere === void 0) { _idRecupere = 0; }
        if (_dateEmprunt === void 0) { _dateEmprunt = null; }
        if (_dateRetour === void 0) { _dateRetour = null; }
        var dateManager = utils_2.SystemDateManager.instance;
        this.id = _idRecupere ? _idRecupere : utils_1.generateUniqueId(); //media.id + emprunteur.id + dateManager.formatDateToString('jj/mm/aaaa');
        this.dateEmprunt = (_dateEmprunt) ? _dateEmprunt : dateManager.date;
        this.dateRetour = (_dateRetour) ? _dateRetour : dateManager.addDays(7).date;
        this.idMedia = _idMedia;
        this.idEmprunteur = _idEmprunteur;
        // l'index de recherche pourrait également contenir la date d'emprunt mais dans la mesure ou un media ne peut être emprunter qu'une seule fois par un emprunteur l'id du media et de l'emprunteur suffisent
        this.indexPourRecherche = (_idMedia + _idEmprunteur).toString();
    }
    //Retourne une description complète de l'emprunt avec : le media qui est concerné, sa date d'emprunt et de retour, l'auteur de l'emprunt.
    Emprunt.prototype.getDescription = function () {
        utils_2.SystemDateManager.instance.date = this.dateEmprunt;
        //todo : permettre de rechercher par identifiant directement et pas forcément indextRecherche
        var mediaEmprunte = acteurs_1.Mediatheque.getInstance().getMedia(this.idMedia);
        var emprunteur = acteurs_1.Mediatheque.getInstance().getEmprunteur(this.idEmprunteur);
        return "Description de l'emprunt :\n        Media concern\u00E9 : " + mediaEmprunte.getDescription() + " \n        Il a \u00E9t\u00E9 emprunt\u00E9 le " + utils_2.SystemDateManager.instance.changeDate(this.dateEmprunt).formatDateToString("jj/mm/aaaa") + " \n            par : " + emprunteur.getDescription() + " \n        et sera a retourner avant le " + utils_2.SystemDateManager.instance.changeDate(this.dateRetour).formatDateToString("jj/mm/aaaa à hh:mn:ss");
    };
    return Emprunt;
}());
exports.Emprunt = Emprunt;
//# sourceMappingURL=medias.js.map