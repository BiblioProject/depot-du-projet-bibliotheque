"use strict";
//namespace MediaManagement { // le namespace risque de disparaître mais je testes le fonctionnement
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
// a voir si on le laisse là ou ailleurs, l'idée serait de la rendre accessible à la fois aux classes qui héritent de Media 
// et à l'UI qui fournirait à l'utilisateur l'ensemble des valeurs de saisie possible à l'utilisateur 
// (une liste déroulante en sortant du cadre du projet)
var genresLivre = ["Policer", "Essai", "SF", "Conte", "Roman", "Biographie"]; // pas à la bonne place (test)
var genresCD = ["Pop", "Rock", "Classique", "Jazz", "Rap"]; // pas à la bonne place (test)
var genresDVD = ["Comédie", "Drame", "Thriller", "Action", "SF"]; // pas à la bonne place (test)
var Media = /** @class */ (function () {
    function Media(type, titre, nbExemplaires) {
        /* en fait l'identifiant du média est supplémentaire à l'isbn ou disc Id, il permet d'identifier un élément en interne */
        this.id = generateUniqueId();
        this.type = type;
        this.titre = titre;
        this.nbExemplaires = nbExemplaires;
    }
    // protected modeleMedia: //Quel est le modèle -> livre, cd dvd ? type string ?
    // c'est le 1er attribut type qui stocke cette valeur
    Media.prototype.getDescription = function () {
        // utilisation de litteraux de gabarits ou string template, sucre synthaxique fourni par Typescript
        return "Ce media est un " + this.type + ". Son titre est " + this.titre;
    };
    Media.prototype.getDispo = function () {
        return this.nbExemplaires;
    };
    return Media;
}());
var Livre = /** @class */ (function (_super) {
    __extends(Livre, _super);
    /* - id : Un ISBN comme identifiant unique // voir si le fait que l'id soit l'ISBN peut être simplement indiqué en signature du constructeur ou si réelle utilité de le différencier de l'id général de la classe mère
    */
    function Livre(isbn, titre, nbExemplaires, auteur, genre, maisonEdition, nbPage) {
        var _this = _super.call(this, "livre", titre, nbExemplaires) || this;
        _this.ISBN = isbn;
        _this.auteur = auteur;
        _this.genre = genre;
        _this.maisonEdition = maisonEdition;
        _this.nbPage = nbPage;
        return _this;
    }
    Livre.prototype.setISBN = function (isbn) {
        this.ISBN = isbn;
    };
    Livre.prototype.getDescription = function () {
        return _super.prototype.getDescription.call(this) + " " + this.maisonEdition + " " + this.ISBN + " " + this.nbPage + " quand dites-vous ?";
    };
    Livre.prototype.getDateDispo = function () {
        return new Date();
    };
    return Livre;
}(Media));
exports.Livre = Livre;
/* voir si utilité d'utiliser une interface pour ces 2 attributs, pas sur ...*/
/*interface digitalMedia {
    producteur : String;
    dureeMn : Number;
}*/
/*export class CD extends Media {
    
    private discId : number
    private groupe : string;
    private genre : string;
    private producteur : string
    private dureeMn : number
    
    // le DiscId est un nombre entier, codé sur 32 bits, représenté en hexadécimal sur 8 caractères (au besoin, il faut ajouter des caractères « 0 » en tête).
    constructor(discId : number, groupe : string, genre : string, producteur : string, dureeMn : number) { //maison d'édition == producteur et possibilité de créer un attribut producteur dans la class Media ou nécessité de l'avoir ici ?
        super("cd");

        this.discId = discId;
        this.groupe = groupe;
        this.genre = genre;
        this.producteur = producteur;
        this.dureeMn = dureeMn;
    }

    getDescription() : string {
        return this.titre;
    }

    getDispo() : number {
        return this.nbExemplaires;
    }

    getDateDispo() : Date {
        return new Date();
    }
}*/
/*class DVD extends Media {
    type = "DVD";
}*/
// comprendre ce que fait exactement cette fonction
function generateUniqueId() {
    var uid1 = Date.now();
    var uid2;
    while ((uid2 = Date.now()) == uid1)
        ;
    return uid2;
}
//}
//# sourceMappingURL=media-version-avancee-brice.js.map