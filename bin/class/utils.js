"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Fonction permettant de générer un identifiant unique
 * @param {number}  Date.now : date du jour au format dd/mm/aaaa hh/mm/ss
 * @return {number}  uid2 : Identifiant unique
 */
function generateUniqueId() {
    var uid1 = Date.now();
    var uid2;
    while ((uid2 = Date.now()) == uid1)
        ;
    return uid2;
}
exports.generateUniqueId = generateUniqueId;
/**
 * Fonction permettant d'envoyer un mail
 * @param {string}  address - subject - content : Permet d'ajouter une adresse de destinataire ; un sujet ; un corps de texte.
 * @return {string} Confirmation d'envoi.
 */
function sendMail(address, subject, content) {
    var mail = "Mail from: mediatheque@lecnam.net\n    To: " + address + "\n    Subject: " + subject + "\n    \n    " + content;
    console.log(mail);
}
exports.sendMail = sendMail;
/*export class Descriptible {

}*/
/* test : non utilisé pour le moment
export interface Iterateur<T>{
    aUnSuivant(): boolean;
    suivant(): T;
}*/
//comment (classe fournie par le prof dans utils.js)
//je l'ai modifiée pour pouvoir modifier _date avec une date d'emprunt (par exmple) récuperée dans un enregistrement du json
//ajout également d'une fonction permettant de proposer différents format de dates mais cette partie va évoluer donc ne pas la commenter pour le moment
var SystemDateManager = /** @class */ (function () {
    function SystemDateManager() {
        this._date = new Date();
    }
    Object.defineProperty(SystemDateManager.prototype, "date", {
        get: function () {
            return new Date(this._date);
        },
        /* méthode ajoutée permettant de remplacer la date instanciée dans le constructeur par une autre date */
        set: function (date) {
            this._date = date;
        },
        enumerable: true,
        configurable: true
    });
    SystemDateManager.prototype.changeDate = function (date) {
        this._date = date;
        return this;
    };
    /* typeFormat == brut, date, dateEtTime verbeux; l'idée n'est pas de donner la main à toutes les options disponibles pour la function toLocaleDateString mais plutôt de définir les quelques formats de dates utilisés dans l'application et de rendre l'implementation simple pour d'éventuels autres developpeurs du projet qui auraient à afficher des dates
    Il serait également possible d'utiliser (réfléchir à solution possibles pour permettre à un developpeur de surcharger cette méthode et de créer un nouveau format)
    ex :
    - brut : 01012020100000 pour 01/01/2020 10h00mn00seconde (utilisé pour générer un identifiant composé comme par ex. l'identidiant d'un emprunt)
    - minimal :
    */
    /* possibilité d'utiliser la bliotheque moment.js dasn un projet ayant une utilisation plus importante des dates */
    /**
     * Méthode permettant de formatter une date en string selon différents formats
     * @param {string}  typeFormat - Format à utiliser pour la date retournée. Valeurs possibles : "time" / "jj/mm/aaaa"
     * @param {boolean=} afficherHeure - Ajouter l'heure au format hh/mm/ss
     * @return {string} Date retournée au format string
     */
    SystemDateManager.prototype.formatDateToString = function (typeFormat, afficherHeure) {
        if (typeFormat === void 0) { typeFormat = ""; }
        if (afficherHeure === void 0) { afficherHeure = false; }
        // gerer cas ou typeFormat inexistant, format par défaut
        var returnValue = "";
        var optionsToLocaleDateString;
        afficherHeure ? optionsToLocaleDateString = { hour: '2-digit', minute: '2-digit', second: "2-digit" } : optionsToLocaleDateString = {};
        switch (typeFormat) {
            case "time":
                returnValue = (this._date.getTime()).toString();
                break;
            case "jj/mm/aaaa":
                // ES6 standard methode
                Object.assign(optionsToLocaleDateString, { year: 'numeric', month: '2-digit', day: '2-digit' });
                returnValue = this._date.toLocaleDateString("fr-FR", optionsToLocaleDateString);
                break;
        }
        return returnValue;
    };
    SystemDateManager.prototype.addDays = function (days) {
        this._date = new Date(this._date.getTime() + days * 24 * 60 * 60 * 1000);
        return this;
    };
    SystemDateManager.prototype.diffInDaysWith = function (date) {
        return Math.ceil((this._date.getTime() - date.getTime()) / (24 * 60 * 60 * 1000));
    };
    SystemDateManager.instance = new SystemDateManager();
    return SystemDateManager;
}());
exports.SystemDateManager = SystemDateManager;
//# sourceMappingURL=utils.js.map