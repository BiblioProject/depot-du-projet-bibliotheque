Tout d'abord, bienvenue à vous cher controleur des travaux finis (enfin ce sera à vous d'évaluer)

Ce projet a été réalisé en binôme entre Benjamin BRUCKLER et Brice VINCENT.

Nous avons décidé de travailler à l'aide de git pour experimenter le travail en équipe avec un outils de versionning.
Hélas nous ne pouvons pas rendre le projet sous ce format. Mais l'idée était de mettre en place un workflow permettant une collaboration la plus efficace possible.
Sur ce point nous sommes satisfait de l'experience acquise. 
Nous avons utiliser bitbicket comme serveur git et l'extension visual studio "gitlens" comme client visuel pour le lancement des principales commandes de git.

Veuillez trouverez ci-dessous les différents remarques permettant le lancement de l'application.

Environnement visual code studio :
Pour lancer le projet depuis VSC, il suffit juste de laisser le dossier .vscode et le fichier tsconfig à la racine du projet.
Ces 2 fichiers indiquent les parametres de débuguage du projet et permettent un déploiement facilité.
Une fois le dossier de l'application ouverte dans VSC, il suffit d'ouvrir le dichier main.ts et de lancer le debuguage du projet (touche f5)
Les fichier typescript sont alors compilés en javascript dans le dossier bin.

Le code source est organisé de la manière suivante :
- fichier src/main.ts à la racine du projet : permet de lancer les instructions du programme
- dossier src/classes : contient les classes des medias (media.ts), des acteurs (acteur.ts) (classes Mediateque et Emprunteur) ainsi que des aclasses de converssion d'objet Json en instances de classe Media (objectToClassConverteur.ts)
- dossier src/interfaces : contient les interfaces du projet
- dossier src/utils : contient les fonction globales utiles aux différentes classes du projet
- dossier src/bin : dossier d'export des fichier .js compilés par la commande tsc (pas besoin de compilé les sources en ligne de commande, laes fichiers de configuration .vscode/launch.json et tsconfig.json le font de manière automatisée)
- dossier node_modules : comporte les fichiers générés lors de l'installation de la bibliothèque fs permettant la lecture et l'écriture de fichier (pour les données json)
- dossier data : comporte les 3 fichiers permettant de sauvegarder et restaurer les medias, les emprunteurs et les emprunteurs

Nous esperons que la lecture de notre code sera simple et limpide.


