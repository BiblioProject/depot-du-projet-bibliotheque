/**
 * Fonction permettant de générer un identifiant unique
 *
 * @export
 * @returns {number}
 */
export function generateUniqueId(): number {
    let uid1: number = Date.now();
    let uid2: number;
    
    while ((uid2 = Date.now()) == uid1);
    return uid2; 
}

/**
 * Fonction permettant d'envoyer un mail
 * 
 * @export
 * @param {string}  address - adresse de destinataire de l'email
 * @param {string}  subject - sujet de l'email
 * @param {string}  content - contenu textuel de l'email
 */
export function sendMail(address: string, subject: string, content: string) {
    let mail = `Mail from: mediatheque@lecnam.net
    To: ${address}
    Subject: ${subject}
    
    ${content}`;

    console.log(mail);
}

/**
 * Classe permettant des opérations récurrentes sur les dates
 *
 * @export
 * @class SystemDateManager
 */
export class SystemDateManager {
    public static instance: SystemDateManager = new SystemDateManager();

    private _date: Date;

    /**
     *Creates an instance of SystemDateManager.
     * @memberof SystemDateManager
     */
    private constructor() {
        this._date = new Date();
    }

    /**
     * Méthode publique ajoutée permettant de remplacer la date instanciée dans le constructeur par une autre date
     * 
     * @param {Date} date
     * @returns {SystemDateManager}
     * @memberof SystemDateManager
     */
    changeDate(date : Date) : SystemDateManager {
        this._date = date;
        return this;
    }

    /**
     * Retourne la date de SystemDateManager
     *
     * @readonly
     * @type {Date}
     * @memberof SystemDateManager
     */
    get date(): Date {
        return new Date(this._date);
    }

    /**
     * Méthode permettant de formatter une date en string selon différents formats
     * pour un plus gros projet il serait possible d'utiliser une bibliothèque externe type moment.js pour faciliter la gestion des dates
     * 
     * @param {string}  typeFormat - Format à utiliser pour la date retournée. Valeurs possibles : "time" / "jj/mm/aaaa"
     * @param {boolean=} afficherHeure - Ajouter l'heure au format hh/mm/ss
     * @return {string} Date retournée au format string selon typeFormat et afficherHeure
     * @memberof SystemDateManager
     */
    formatDateToString(typeFormat:string = "", afficherHeure:boolean = false) : string {
        // gerer cas ou typeFormat inexistant, format par défaut
        var returnValue : string = "";
        var optionsToLocaleDateString : object;
        afficherHeure ? optionsToLocaleDateString = {hour:'2-digit', minute: '2-digit', second : "2-digit"} : optionsToLocaleDateString = {};
        switch (typeFormat) {
            case "time" :
                returnValue = (this._date.getTime()).toString();
                break;
            case "jj/mm/aaaa" :
                // ES6 standard methode
                Object.assign(optionsToLocaleDateString, {year: 'numeric', month: '2-digit', day: '2-digit'});
                returnValue = this._date.toLocaleDateString("fr-FR",optionsToLocaleDateString);
                break;
        }
         
        return returnValue;
    }

    /**
     * Permet d'ajouter un nombre de jours à l'objet date de SystemDateManager
     *
     * @param {number} days - Nombre de jours à ajouter
     * @returns {SystemDateManager}
     * @memberof SystemDateManager
     */
    addDays(days: number): SystemDateManager {
        this._date = new Date(this._date.getTime() + days * 24 * 60 * 60 * 1000);
        return this;
    }

    /**
     * Retourne la différence en nombre de jour entre la date de SystemDateManager et une autre date passée en paramètre
     *
     * @param {Date} date - date a comparer avec la date de SystemDateManager
     * @returns {number}
     * @memberof SystemDateManager
     */
    diffInDaysWith(date: Date): number {
        return Math.ceil((this._date.getTime() - date.getTime()) / (24 * 60 * 60 * 1000));
    }
}
