import { Media } from "./medias";
import { LivreConverteur, CDConverteur, DVDConverteur, EmpruntConverteur } from "./objectToClassConverteur"; 
import { Emprunt } from "./medias";
import { generateUniqueId, sendMail, SystemDateManager } from "../utils/utils";
import { Identifiable, ChargementDonneesObserver } from "../interfaces/interfaces";
import { EmprunteurConverteur } from "./objectToClassConverteur";

/**
 * Singleton Mediateque prmettant les opérations sur la médiatèque
 *
 * @export
 * @class Mediatheque
 */
export class Mediatheque {
    
    /*
     * Différentes collections permettant de stocker en mémoire les médias, emprunteurs et emprunts
     * Initialement, nous avions testé une approche différente
     * Nous nous sommes dit que puisque la mediathèque était censée manipuler des milliers de medias et d'emprunteurs,
     * il serait trop couteux en performance de parcourir à chaque fois toutes instances de médias et d'emprunteur en mémoire via la boucle for d'Array
     * L'idée était d'utiliser des clés string uniques pour accéder rapidement à une instance de médias dans une collection
     * ex : delete collectionObjetEmprunteur['email de l emprunteur'] idem pour l'accès rapide à l'élément d'une collection ex : return collectionMedia['isbn ou disc id d'un média'] 
     * L'inconvénient de cette solution c'est que les méthodes natives des Array (filter, map, ...) n'étaient pas applicable à des Array avec des clés de type string.
     * De plus la méthode length ne comptait pas ces éléments et ne s'applique que pour des clés de type number.
     * Au final, nous nous sommes rendus compte qu'il n'était pas forcément nécessaire d'optimiser l'accès aux instances des collections
     * puisque c'est en pratique la base de données qui retourne un nombre limité d'enregistrement et que les collection en mémoire utilisent donc un nombre limité d'elements
     * Nous avons également tester l'utilisation de Map pour les collections mais elles sont incompatibles avec l'encodage et le décodage json.
     * Nous avons donc décidé d'utiliser des Array typés.
     */

    private collectionMedia : Array<Media> = new Array();
    private collectionEmprunteur : Array<Emprunteur> = new Array();
    private collectionEmprunt : Array<Emprunt> = new Array();

    // observer qui observe le chargement des données de l'application et execute les séquences nécessaires une fois le chargement terminé
    private observerDonneesChargees: ChargementDonneesObserver = null;

    // implémentation du singleton Mediateque
    private static _instance: Mediatheque;

    // liste des fichiers permettant la sauvegarde et la restauration des données
    private readonly fileMedias : string = 'data/medias.txt';
    private readonly fileEmprunteurs : string = 'data/emprunteurs.txt';
    private readonly fileEmprunts : string = 'data/emprunts.txt';
    
    /*Implémentation du singleton Mediatheque avec une méthode statique permattant d'accèder directement à l'instance sans instancier une nouvelle instance de classe Mediatheque*/
    public static getInstance() {
        return this._instance || (this._instance = new this());
    }

    /**
     * Méthode d'enregistrement de l'observeur sur la médiathèque
     *
     * @param {ChargementDonneesObserver} new_observer
     * @memberof Mediatheque
     */
    public registerObserver(new_observer: ChargementDonneesObserver){
        this.observerDonneesChargees = new_observer;
    }

   /**
    * Méthode appellée à plusieurs reprise et déclenchée lorsqu'un fichier de données est chargé
    *
    * @private
    * @param {string} fichierCharge - Indique le fichier qui vient d'être chargé 
    * (permet de charger les fichiers suivants pour lancer à terme la méthode finished de l'observeur 
    * qui indique que toutes les données de l'application ont été chargées et que cette dernière 
    * peut lancer la séquence d'opération sur la mediateque
    * Les medias sont chargés en 1er, ensuite les emprunteurs et ensuite seulement les emprunts 
    * (les emprunts nécessitent que les données des medias et emprunteurs soient chargés pour être instanciés)
    * Il aurait également été possible d'utiliser des promise et la méthode promise.all pour controler le chargement des 3 fichiers 
    * en processus parrallèle mais l'utilisation d'un design pattern observeur a été testé dans ce projet 
    * 
    * @memberof Mediatheque
    */
   private notifyObserver(fichierCharge : string){
        if (this.observerDonneesChargees){
            if (fichierCharge===this.fileMedias)
                this.restaurerDonnees(this.fileEmprunteurs);
            if (fichierCharge===this.fileEmprunteurs)
                this.restaurerDonnees(this.fileEmprunts);
            if (fichierCharge===this.fileEmprunts)
                this.observerDonneesChargees.finished();
        }
    }

    /**
     * Lance le charrgement du 1er fichier this.fileMedias
     *
     * @memberof Mediatheque
     */
    chargementDonneesApplication() {
        // une fois le chargement des medias effectué l'obseveur sera notifié avec le nom du fichier chargé, 
        // il chargera ensuite les emprunteurs puis les emprunts et alors il lancera la méthode finished de l'observeur 
        // pour lancer la séquence principale de l'application une fois les données toutes chargées
        this.restaurerDonnees(this.fileMedias);
    }

    /**
     * Méthode qui lit un fichier text contenant les différentes collections de Médias, emprunteurs et emprunts et les instancie en mémoire
     *
     * @param {string} fichier - nom du fichier à lire
     * @memberof Mediatheque
     */
    restaurerDonnees (fichier: string) {
        const fs = require('fs');

        fs.readFile(fichier, (err, donnees) => {
            if (err) throw err;
            if (donnees.length!=0) {

                // la méthode Json.parse retourne une collection d'objet
                let collectionObject = JSON.parse(donnees);
                
                // pour chaque objets présents dans la collection en cours
                for (var objectJson of collectionObject) {
                    // on connait le type des éléments chargés (média, emprunteurs ou emprunt) avec le nom du fichier chargé
                    // les classes présentes dans objectToClassConverteur permettent une conversion rapide des objets récuperés par json en instance de classe media ou emprunteur ou emprunt 
                    if (fichier===this.fileMedias) {
                        // chaque objet de la collection possède une propriété type (provenant de l'attribue type des classes filles de Media)
                        switch (objectJson.type) {
                            case "livre" :
                                this.ajoutMedia(new LivreConverteur(objectJson).conversionClasse());
                                break;
                            case "CD" :
                                this.ajoutMedia(new CDConverteur(objectJson).conversionClasse());
                                break;
                            case "DVD" :
                                this.ajoutMedia(new DVDConverteur(objectJson).conversionClasse());
                                break;
                        }   
                    } else if (fichier===this.fileEmprunteurs) {
                        this.ajoutEmprunteur(new EmprunteurConverteur(objectJson).conversionClasse());
                    } else if (fichier===this.fileEmprunts) {
                        this.ajoutEmprunt(new EmpruntConverteur(objectJson).conversionClasse());
                    }
                }
            }
            // déclenchement du notificateur de l'oberveur pour lancer la séquence suivante
            Mediatheque.getInstance().notifyObserver(fichier);
        });

    }

    /**
     * Permet de sauvegarder l'ensemble des collections (media, emprunteurs et emprunt dans les fichiers text correspondants)
     *
     * @memberof Mediatheque
     */
    public sauvegarderDonneesApplication() {
        this.sauvegarderCollection(this.collectionMedia, this.fileMedias);
        this.sauvegarderCollection(this.collectionEmprunteur, this.fileEmprunteurs);
        this.sauvegarderCollection(this.collectionEmprunt, this.fileEmprunts);
    }

    /**
     * Utilisation d'une contrainte générique permettant de sauvegarder une collection peut importe le type d'éléments qu'elle contient
     *
     * @private
     * @template T
     * @param {Array<T>} collection - de type Media, Emprunteur ou Emprunt
     * @param {string} fichier - chemin du fichier ou sera sauvegardé la collection en format json
     * @memberof Mediatheque
     */
    private sauvegarderCollection <T> (collection: Array<T>, fichier: string ) {
        const fs = require('fs');
        // utilisation de la méthode JSON.stringify() pour convertir une collection en une représentation textuelle
        let elementsJson = JSON.stringify(collection);
        // et l'écrire dans un fichier dont le chemin est passé en paramètre
        fs.writeFileSync(fichier, elementsJson);    
    }

    /**
     * Utilisation d'une contrainte générique permettant d'ajouter un élément à une collection, peut importe le type de l'élément
     * La contrainte indique également que la collection possède des élément du même type que l'élément à ajouter (ajout)
     *
     * @private
     * @template T
     * @param {Array<T>} collection - collection dans laquelle sera ajouté ajout
     * @param {T} ajout - instance de classe de type Media, Emprunteur ou Emprunt
     * @memberof Mediatheque
     */
    private ajoutCollection<T>(collection: Array<T>, ajout : T) {
        
        collection.push(ajout);
    
        /*
        * Initialement la solution utilisée était la suivante :
        * if (!collection[ajout.id]) collection[ajout.id] = ajout;
        * else console.log(`L'élément ${ajout.constructor.name} "${ajout.id}" est déjà enregistré dans la médiathèque`);
        * 
        * collection était de type object
        * Il était ainsi possible de tester par la même occasion que l'élément n'était pas déjà présent dans la collection
        */
    }

    /**
     * Permet la suppression d'un élément d'une collection
     * L'usage d'une contrainte générique permet d'utiliser cette méthode peut importe le type des éléments de la collection et l'éméent à supprimer
     * La contrainte requiert également que les élément de la collection et le retrait soit du même type
     * L'utilisation de l'interface Identifiable permet d'utiliser l'attribut public id sur retrait qui n'est pas typé
     * On indique par cette ocassion au compilateur que retrait doit posseder un attribut id.
     * Dans un soucis d'optimisation le parcours de la collection se fait dans le sens décroissant des valeurs d'index numérique
     * cela permet de supprimer les éléments en fin de tableau en 1er et ainsi de ne pas décaler les index des éléments précédents (ça aurait été le cas si on était parti du 1er élément pour aller au dernier)
     * lorsque l'élément retrait est trouvé dan la collection et supprimé, on sort de la boucle avec l'instruction break afin de ne pas parcourir inutilement le restant de la collection
     * en effet, un controle est effectué à l'ajout d'un élémnet pour que les éléments inseré dans les collections ne puissent être dupliqués
     * 
     * @template T
     * @param {Array<T>} collection - collection dans laquelle retrait est a retiré
     * @param {T} retrait - instance de classe de type Media, Emprunteur ou Emprunt
     * @memberof Mediatheque
     */
    public suppressionCollection<T extends Identifiable>(collection:Array<T>, retrait : T) {
        for (var i = collection.length - 1; i >= 0; i--) {
            let element : T = collection[i];
            if (element.id == retrait.id) {
                this.collectionMedia.splice(i, 1);
                //.constructor.name permet d'avoir le nom de classe de l'instance à l'execution du programme
                console.log(`L'${retrait.constructor.name} ${retrait.id} a été supprimé de la médiathèque`);
                break; //sortie de la boucle une fois l'élément supprimé pour éviter un parcours inutile (uniquement un seul indexIdentique ne peut exister dans la collection)
            }
        }
    }

    /**
     * Ajout d'un média à collectionMedia et controle préalable que l'élément n'y est pas déjà
     * via l'attribut indexPourRecherche qui correspond à l'ISBN, au discId et à l'EIRD en fonction du type de média à ajouter
     *
     * @param {Media} ajout - Instance de Media a ajouter à collectionMedia
     * @memberof Mediatheque
     */
    public ajoutMedia(ajout : Media) {
        let findedIndex : Array<Media> = this.collectionMedia.filter(media => media.indexPourRecherche == ajout.indexPourRecherche);
        if (findedIndex.length>0) console.log(`Ajout impossible, le média ${ajout.id} existe déjà dans la médiathèque`)
        else this.collectionMedia.push(ajout);
    }

    /**
     * Suppression d'un media de collectionMedia
     *
     * @param {Media} retrait - instance de Media
     * @memberof Mediatheque
     */
    public suppressionMedia(retrait : Media) {
        this.suppressionCollection(this.collectionMedia, retrait);
    }

    /**
     * Ajout d'un emprunteur à collectionEmprunteur et controle préalable que l'élément n'y est pas déjà
     * via l'attribut indexPourRecherche qui correspond à l'ISBN, à l'email de l'emprunteur à ajouter
     * 
     * @param {Emprunteur} ajout - instance d'Emprunteur à ajouter
     * @memberof Mediatheque
     */
    public ajoutEmprunteur(ajout : Emprunteur) {
        let findedIndex : Array<Emprunteur> = this.collectionEmprunteur.filter(emprunteur => emprunteur.indexPourRecherche == ajout.indexPourRecherche);
        if (findedIndex.length>0) console.log(`Ajout impossible, l'emprunteur ${ajout.id} existe déjà dans la médiathèque`)
        else this.collectionEmprunteur.push(ajout);
    }

    /**
     * Suppression d'un Emprunteur de collectionEmprunteur
     *
     * @param {Emprunteur} retrait
     * @memberof Mediatheque
     */
    public suppressionEmprunteur(retrait : Emprunteur) {
        this.suppressionCollection(this.collectionEmprunteur, retrait);
    }

    /**
     * Retourne la prochaine date de disponibilité d'un media
     * Cette méthode filtre une 1ere fois les medias recherché dans la collection puis retourne la date minimal des médias trouvés
     *
     * @param {Media} media
     * @returns {Date} retourne la date minimale si le media a été trouvé ou null dans le cas contraire
     * @memberof Mediatheque
     */
    public getDateDispoMedia(media : Media) : Date {
        let result : Array<Emprunt> = this.collectionEmprunt.filter(emprunt => emprunt.idMedia === media.id)
        let minDate : Date = null
        for(let i = 0; i < result.length; i++){
            if (i==0) minDate = result[i].dateRetour; 
            else if (result[i].dateRetour<minDate) minDate=result[i].dateRetour;
        }
        return minDate;
    }

    /**
     * Méthode qui teste la possibilité de l'ajout d'un emprunt
     * et l'ajoute si l'emprunt à été validé par la classe Emprunteur
     *
     * @param {Media} media
     * @param {Emprunteur} emprunteur
     * @memberof Mediatheque
     */
    public demandeAjoutEmprunt(media : Media, emprunteur : Emprunteur) {
        // si le media et l'emprunteur sont bien existants
        if (media&&emprunteur) {
            // emprunterMedia retourne un Emprunt si l'emprunteur peut emprunter ce media
            let empruntValide : Emprunt = emprunteur.emprunterMedia(media);
            // ajout de l'emprunt dans la collection de la mediatheque et de l'emprunteur si l'emprunt est validé pour l'emprunteur
            if (empruntValide) this.ajoutEmprunt(empruntValide);
        } else {
            console.log("Le média ou l'emprunteur utilisé pour la demande d'éjout du média n'est pas valide");
        }
    }

    /**
     * Ajoute un emprunt à collectionEmprunt, rafraichit la liste des emprunts de l'emprunteur et retire un exemplaire du media
     *
     * @param {Emprunt} emprunt
     * @memberof Mediatheque
     */
    public ajoutEmprunt(emprunt : Emprunt) {
        let emprunteur : Emprunteur = this.getEmprunteur(emprunt.idEmprunteur);
        let media : Media = this.getMedia(emprunt.idMedia);

        this.collectionEmprunt.push(emprunt);
        this.majCollectionEmpruntEmprunteur(emprunteur);
        media.retirerExemplaire();

        console.log(`L'emprunteur ${emprunt.idEmprunteur} vient d'emprunter le media ${emprunt.idMedia}`);
    }

    /**
     * Supprime un emprunt de collectionEmprunt, rafraichit la liste des emprunts de l'emprunteur et ajoute un exemplaire du media
     *
     * @param {Emprunt} emprunt
     * @memberof Mediatheque
     */
    public retourEmprunt(emprunt : Emprunt) {
        let emprunteur : Emprunteur = this.getEmprunteur(emprunt.idEmprunteur);
        let media : Media = this.getMedia(emprunt.idMedia);

        this.suppressionCollection(this.collectionEmprunt, emprunt);
        this.majCollectionEmpruntEmprunteur(emprunteur);
        media.ajoutExemplaire();

        console.log(`L'emprunteur ${emprunt.idEmprunteur} vient de retourner le media ${emprunt.idMedia}`)
    }

    /**
     * Rafraichissement de la liste des emprunt de l'emprunteur
     * Pour garder une cohérence avec collectionEmprunt, cette dernière est filtrée avec les emprunts correspondants à l'emprunteur passé en parametre
     *
     * @private
     * @param {Emprunteur} emprunteur
     * @memberof Mediatheque
     */
    private majCollectionEmpruntEmprunteur(emprunteur : Emprunteur) {
        emprunteur.updateCollectionEmprunt(this.getEmpruntByEmprunteur(emprunteur));
    }

    /**
     * Filtre collectionEmprunt en fonction de l'emprunteur
     *
     * @param {Emprunteur} emprunteur
     * @returns {Array<Emprunt>}
     * @memberof Mediatheque
     */
    public getEmpruntByEmprunteur(emprunteur : Emprunteur) : Array<Emprunt> {
        return this.collectionEmprunt.filter(emprunt => emprunt.idEmprunteur === emprunteur.id)
    }

    /**
     * Utilisation d'une function overload (concept à la base dans C#) permettant d'utiliser une même méthode avec des parametres de différents types
     * Ici pour obtenir un media on peut soit passé un string (ISBN, discId ou EIRD) ou son identifiant (number)
     *
     * @param {string} identificateur
     * @returns {Media}
     * @memberof Mediatheque
     */
    public getMedia(identificateur : string) : Media;
    public getMedia(identificateur : number) : Media;
    public getMedia(identificateur : string | number) : Media {
        if (typeof identificateur === "string")
            return this.getElementInCollectionByIndexrecherche(this.collectionMedia, identificateur)
        else return this.getElementInCollectionById(this.collectionMedia, identificateur);
    }

    /**
     * Utilisation d'une function overload (concept à la base dans C#) permettant d'utiliser une même méthode avec des parametres de différents types
     * Ici pour obtenir un emprunteur on peut soit passé un string (email de l'emprunteur) ou son identifiant (number)
     *
     * @param {string} identificateur
     * @returns {Media}
     * @memberof Mediatheque
     */
    public getEmprunteur(identificateur : string) : Emprunteur;
    public getEmprunteur(identificateur : number) : Emprunteur;
    public getEmprunteur(identificateur : string | number) : Emprunteur {
        return typeof identificateur === "string"
        ? this.getElementInCollectionByIndexrecherche(this.collectionEmprunteur, identificateur)
        : this.getElementInCollectionById(this.collectionEmprunteur, identificateur);
    }

    /**
     * Recherche dans une collection<T> (généricité) de l'attribut indexPourrecherche passé en parametre 
     *
     * @private
     * @template T
     * @param {Array<T>} collection
     * @param {string} indexRecherche - email, isbn, discId ou EIRD 
     * @returns {T}
     * @memberof Mediatheque
     */
    private getElementInCollectionByIndexrecherche<T extends Identifiable>(collection:Array<T>, indexRecherche:string) : T {
        return collection.find(element => element.indexPourRecherche === indexRecherche);
    }

    /**
     * Retourne l'élément correspondant à l'identifiant passé en parametre
     * find ne retourne qu'un résultat. C'est cohérent puisque à l'ajout dans une collection, on s'assure qu'un élément ne soit pas dupliqué.
     *
     * @private
     * @template T
     * @param {Array<T>} collection
     * @param {number} id
     * @returns {T}
     * @memberof Mediatheque
     */
    private getElementInCollectionById<T extends Identifiable>(collection:Array<T>, id:number) : T {
        return collection.find(element => element.id === id);
    }

    /**
     *Notifie par mail l'ensemble des emprunteurs ayant des emprunts en retard
     *
     * @memberof Mediatheque
     */
    public envoyerEmailEmprunteurEnRetard() {
        var dateManager : SystemDateManager = SystemDateManager.instance;
        for (var i = this.collectionEmprunt.length - 1; i >= 0; i--) {
            let emprunt : Emprunt = this.collectionEmprunt[i];
            let emprunteur : Emprunteur = this.getEmprunteur(emprunt.idEmprunteur);
            let media : Media = this.getMedia(emprunt.idMedia);
            
            if (emprunt.dateRetour<dateManager.date) sendMail(emprunteur.email, `Retard mediatheque de ${dateManager.diffInDaysWith(emprunt.dateRetour)} jour(s)`, `Merci de rapporter le media suivant : ${media.getDescription()}`);
        }
    }

}

/**
 * Classe des emprunteurs
 *
 * @export
 * @class Emprunteur
 */
export class Emprunteur {

    public readonly id : number;
    private readonly nom : string;
    private readonly prenom : string;
    public readonly email : string;
    public collectionEmprunt : Array<Emprunt>;
    public readonly indexPourRecherche : string;

    constructor(_idEnregistre: number = 0, nom : string, prenom : string, email : string) {
        this.id = _idEnregistre ? _idEnregistre : generateUniqueId();
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        //this.collectionMediaEmprunte = _collectionMediaEmprunte;
        this.indexPourRecherche = email;
        this.collectionEmprunt = [];
    }

    /**
     * mise à jour de la liste d'emprunt à chaque ajout d'un emprunt par la mediatheque
     * en récuperant la liste des emprunts d'un utilisateur depuis la collection d'emprunt de la mediatheque 
     * on s'assure de la cohérence entre les 2 collections.
     *
     * @param {Array<Emprunt>} collection
     * @memberof Emprunteur
     */
    updateCollectionEmprunt(collection : Array<Emprunt>) {
        this.collectionEmprunt = collection;
    }

    /**
     * Retourne la description de l'emprunteur
     *
     * @returns {string}
     * @memberof Emprunteur
     */
    getDescription() : string {
        return `Description de l'emprunteur : 
        - nom : ${this.nom}
        - prenom : ${this.prenom}
        - email : ${this.email}`;
    }

    /**
     * Retourne un Emprunt si l'emprunteur peut emprunter le media passé en parametre ou null sinon.
     *
     * @param {Media} media
     * @returns {Emprunt}
     * @memberof Emprunteur
     */
    emprunterMedia(media: Media) : Emprunt {
        if (this.peutEmprunterMedia(media))
            return new Emprunt(0, media.id,this.id);
        else return null;
    }

    /**
     * Teste si l'emprunteur à emprunter moins de 3 médias et si l'exemplaire qu'il souhaite est disponible
     *
     * @private
     * @param {Media} media
     * @returns {boolean}
     * @memberof Emprunteur
     */
    private peutEmprunterMedia(media : Media):boolean {
        if (this.collectionEmprunt.length<3) {
            if (!this.emprunteDejaMedia(media)) {
                if (media.getNbExemplairesDispo()>0)
                    return true;
                else {
                    var dateManager : SystemDateManager = SystemDateManager.instance;
                    var dateRetourFormatte : string = dateManager.changeDate(media.getDateDispo()).formatDateToString("jj/mm/aaaa");
                    console.log(`Le ${media.constructor.name} "${media.id}" n'est plus disposible pour l'emprunt. Il sera a nouveau disponbible le ${dateRetourFormatte}`);
                    return false;
                }
            }else {
                console.log(`L'emprunteur ${this.id} doit déjà rendre le ${media.constructor.name} ${media.id}.`);
                return false;
            }
                
        }else {
            console.log(`L'emprunteur ${this.id} a atteint le nombre maximum d'emprunt. Il doit rendre un emprunt pour en emprunter un nouveau`)
            return false;
        }         
    }

    /**
     * Retourne true si l'emprunteur emprunte déjà le média passé en parametre, false sinon
     *
     * @private
     * @param {Media} mediaAEmprunter
     * @returns {boolean}
     * @memberof Emprunteur
     */
    private emprunteDejaMedia(mediaAEmprunter : Media) :boolean {
        let mediatheque = Mediatheque.getInstance();
        let mediaDejaEmprunte = this.collectionEmprunt.find( emprunt => emprunt.idMedia === mediaAEmprunter.id )
        return mediaDejaEmprunte===undefined ? false : true;
    }
}