import { Emprunteur, Mediatheque } from "./acteurs";
import { generateUniqueId } from "../utils/utils";
import { Identifiable } from "../interfaces/interfaces";
import { SystemDateManager } from "../utils/utils";

/**
 * Classe mère permettant de faire hériter les classes filles Livre, CD, DVD
 *
 * @export
 * @abstract - Cette classe abstraite ne peut être instanciée directement
 * @class Media
 * @implements {Identifiable}
 */
export abstract class Media implements Identifiable {

    public readonly id: number;
    public readonly type: string;
    protected readonly titre: string;
    protected nbExemplaires: number;
    //sert à rechercher un media à partir d'une reference unique avant l'ajout (ISBN, discID, EIRD)
    public readonly indexPourRecherche: string;

    //L'idEnregistré est fourni quand on crée un Media suite au chargement des données de l'application avec les MediaConverteur
    constructor(_idEnregistre: number = 0, _type : string, _titre : string, _nbExemplaires : number, _indexPourRecherche : string) {
        this.id = _idEnregistre ? _idEnregistre : generateUniqueId();
        this.type = _type;
        this.titre = _titre;
        this.nbExemplaires = _nbExemplaires;
        this.indexPourRecherche = _indexPourRecherche;
    }

    /*  
	 * Retourne la description d'un média, méthode surchargée dans les classes filles Cd, DVD, Livre
	 */
    getDescription () : string {
        // utilisation de litteraux de gabarits ou string template, sucre synthaxique fourni par Typescript
        return `Ce media est un ${this.type}. Son id est ${this.id}. Son titre est ${this.titre}. Il reste ${this.nbExemplaires} exemplaire(s).`;
    }

    /**
     * retourne le nb d'exemplaire disponible pour ce média
     *
     * @returns {number}
     * @memberof Media
     */
    public getNbExemplairesDispo () : number {
        return this.nbExemplaires;
    }

    /**
     * Méthode faisant appel à la méthode getDateDispoMedia de la mediateque pour connaitre la prochaine date de disponibilité dudit média 
     *
     * @returns {Date}
     * @memberof Media
     */
    public getDateDispo () : Date {
        // on déleque cette tâche a la médiateque car c'est elle qui gère la collection des emprunts indiquant les dates de retour des medias 
        return Mediatheque.getInstance().getDateDispoMedia(this);
    }

    /**
     * Ajoute un exemplaire au média en cours
     *
     * @memberof Media
     */
    public ajoutExemplaire() {
        console.log(`Le nombre d'exemplaire de média ${this.id} est passé de ${this.nbExemplaires} à ${this.nbExemplaires+=1}`);
    }

    /**
     * Retire un exemplaire ua media en cours
     *
     * @memberof Media
     */
    public retirerExemplaire() {
        console.log(`Le nombre d'exemplaire de média ${this.id} est passé de ${this.nbExemplaires} à ${this.nbExemplaires-=1}`);
    }
}

 /*
 * Calsse fille héritée de Media permettant d'instancier un Livre.
 *
 * @export
 * @class Livre
 * @extends {Media}
 */
export class Livre extends Media {
    
    private readonly ISBN : string;
    private readonly auteur : string;
    private readonly genre : string;
    private static genresLivre : Array<String> = ["Policer","Essai","SF","Conte","Roman","Biographie"];
    private readonly maisonEdition : string;
    private readonly nbPage : number;

    /**
     * Creer nune instance de Livre hérité de Media
     * @param {number} [_idEnregistre=0] - Si ce parametre est connu (cas de chargement d'un media existant enregistré depuis un fichier texte) alotrs on le fournit ici, sinon on donne la valeur 0 s'il s'agit de la création d'un nouveau média
     * @param {string} _isbn - L'International Standard Book Number (ISBN) ou Numéro international normalisé du livre est un numéro internationalement reconnu, créé en 1970, identifiant de manière unique chaque édition de chaque livre publié
     * @memberof Livre
     */
    constructor(_idEnregistre:number = 0, _isbn : string, _titre : string, _nbExemplaires : number, _auteur : string, _genre : string, _maisonEdition : string, _nbPage : number) { //maison d'édition == producteur et possibilité de créer un attribut producteur dans la class Media ou nécessité de l'avoir ici ?
        // appel du constructeur de Media pour le constructeur des attributs hérités
        super(_idEnregistre, "livre", _titre, _nbExemplaires, _isbn);
        
        this.auteur = _auteur;
        this.genre = _genre;
        //todo tester que le genre est présent dans le genresArray
        this.maisonEdition = _maisonEdition;
        this.nbPage = _nbPage;
        this.ISBN = _isbn;
    }

    /**
     * Retourne la description d'un livre
     * @return {string} La maison d'edition, l'auteur, le genre, l'ISBN et le nombre de pages.
     */
    getDescription() : string {
        return `${super.getDescription()} Sa maison d'édition est ${this.maisonEdition}. Son auteur est ${this.auteur}. Son genre est ${this.genre}. Son ISBN est ${this.ISBN}. Il possède ${this.nbPage} page(s)`;
    }

}

/**
 * Classe fille de la classe mère "media" permettant d'instancier un CD.
 * @attribut {number} discID : Le DiscId est un identifiant calculable pour tout CD Audio. Cet identifiant est un nombre dont la valeur dépend du nombre de pistes sur le CD, et de la position de chaque piste. Unique pour chaque édition de cd
 */
export class CD extends Media {
    
    private readonly discID : string 
    private readonly groupe : string;
    private readonly genre : string;
    private static  genresCD : Array<String> = ["Pop","Rock","Classique","Jazz","Rap"]; 
    private readonly producteur : string
    private readonly dureeMn : number 

    constructor(_idEnregistre : number = 0, _discID : string, _titre : string, _nbExemplaires:number, _groupe : string, _genre : string, _producteur : string, _dureeMn : number) {
        super(_idEnregistre, "CD", _titre, _nbExemplaires, _discID);

        this.groupe = _groupe;
        this.genre = _genre;
        this.producteur = _producteur;
        this.dureeMn = _dureeMn;
        this.discID = _discID;
    }

    // Retourne la description d'un CD avec le nom du producteur, son discID, son genre, son groupe et sa durée.
    getDescription() : string {
        return `${super.getDescription()} Son producteur est ${this.producteur}. Son discID est ${this.discID}. Son genre est ${this.genre}. Son groupe est ${this.groupe}. Sa durée est ${this.dureeMn} mn.`;
    }

}

/**
 * Classe fille de la classe mère "media" permettant d'instancier un DVD.
 * @attribut {number} EIRD : Identifiant unique d'un DVD
 */
export class DVD extends Media {
    
    private readonly EIRD : string;
    private readonly realisateur : string;
    private readonly genre : string;
    private static genresDVD : Array<String> = ["Comédie","Drame","Thriller", "Action","SF"]; 
    private readonly producteur : string;
    private readonly dureeMn : number;
    
    constructor(_idEnregistre : number = 0, _EIRD : string, _titre: string, _nbExemplaires:number, _realisateur : string, _genre : string, _producteur : string, _dureeMn : number) { 
        super(_idEnregistre, "DVD", _titre, _nbExemplaires, _EIRD);
        this.realisateur = _realisateur;
        this.genre = _genre;
        this.producteur = _producteur;
        this.dureeMn = _dureeMn;
        this.EIRD = _EIRD;
    }

	//Retourne la description d'un DVD avec son EIRD, son réalisateur, son genre, son producteur et sa durée.
    getDescription() : string {
        return `${super.getDescription()} Son EIRD est ${this.EIRD}. Son réalisateur est ${this.realisateur}. Son genre est ${this.genre}. Son producteur est ${this.producteur}. Sa durée est de ${this.dureeMn} mn.`;
    }

}

/**
 * Classe mère permettant d'instancier un emprunt.
 * @attribut {number} id : attribut généré via la fonction generateUniqueId.
 * @attribut {number} idMedia : identifiant du média que ce soit un livre un cd ou un dvd.
 * @attribut {number} idEmprunteur : identiifant de l'emprunteur.      
 * @attribut {Date} dateEmprunt : date d'emprunt du média
 * @attribut {Date} dateRetour : date de retour prévu de l'emprunt (j+7 de la date d'emprunt)  
 * @attribut {string} indexPourRecherche : l'index de recherche pourrait également contenir la date d'emprunt mais dans la mesure ou un media ne peut être emprunter qu'une seule fois par un emprunteur l'id du media et de l'emprunteur suffisent             
*/
export class Emprunt implements Identifiable {

    public readonly id : number; // voir si pas moyen de grouper la gestion de l'id dasn classe à implémentée car redondant entre media et Emprunt
    public readonly idMedia : number;
    public readonly idEmprunteur : number;
    public readonly dateEmprunt : Date;
    public readonly dateRetour : Date;
    public readonly indexPourRecherche : string;

    /* les dates servent uniquement pour le chargement des emprunts enregistrées mais pas lors de la création d'un nouvel emprunt qui utilise la date du jour dans le constructor */
    constructor(_idRecupere : number = 0, _idMedia : number, _idEmprunteur : number, _dateEmprunt : Date = null, _dateRetour : Date = null) {

        var dateManager : SystemDateManager = SystemDateManager.instance;

        this.id = _idRecupere ? _idRecupere : generateUniqueId(); //media.id + emprunteur.id + dateManager.formatDateToString('jj/mm/aaaa');
        this.dateEmprunt = (_dateEmprunt) ? _dateEmprunt : dateManager.date;
        this.dateRetour = (_dateRetour) ? _dateRetour : dateManager.addDays(7).date;
        
        this.idMedia = _idMedia;
        this.idEmprunteur = _idEmprunteur;

        this.indexPourRecherche = (_idMedia + _idEmprunteur).toString();

    }

    //Retourne une description complète de l'emprunt avec : le media qui est concerné, sa date d'emprunt et de retour, l'auteur de l'emprunt.
    getDescription() : string {
        
        let mediaEmprunte = Mediatheque.getInstance().getMedia(this.idMedia);
        let emprunteur = Mediatheque.getInstance().getEmprunteur(this.idEmprunteur);

        /* ajout d'une méthode formatDateToString à SystemDateManager pour retourner dans la description au format string */
        return `Description de l'emprunt :
        Media concerné : ${mediaEmprunte.getDescription()} 
        Il a été emprunté le ${SystemDateManager.instance.changeDate(this.dateEmprunt).formatDateToString("jj/mm/aaaa")} 
            par : ${emprunteur.getDescription()} 
        et sera a retourner avant le ${SystemDateManager.instance.changeDate(this.dateRetour).formatDateToString("jj/mm/aaaa à hh:mn:ss")}`;
    }

}