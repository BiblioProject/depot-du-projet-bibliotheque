import { Emprunteur } from "./acteurs";
import { Livre, Media, CD, DVD, Emprunt } from "./medias";

/**
 * Classe mère permettant de définir les attributs de base d'un Media pour la conversion d'un objet en instance de Media
 * permet de bénéficier de l'auto completion sur les données récuperer au format json et d'alleger la méthode restaurerDonnees de Mediatheque
 *
 * @export
 * @abstract - Cette classe mere abstraite ne peut être instanciée directement
 * @class MediaConverter
 */
export abstract class MediaConverter {
    id: number;
    type: string;
    titre: string;
    nbExemplaires: number;
    indexPourRecherche: string;

    constructor(_objectAConvertir) {
        this.id = _objectAConvertir.id;
        this.type = _objectAConvertir.type;
        this.titre = _objectAConvertir.titre;
        this.nbExemplaires = _objectAConvertir.nbExemplaires;
        this.indexPourRecherche = _objectAConvertir.indexPourRecherche;
    }

    /**
     * Méthode abstraite (à redéfinir dnas les classes filles) permettant de convertir l'objet en média
     *
     * @abstract
     * @returns {Media}
     * @memberof MediaConverter
     */
    abstract conversionClasse() : Media;

}

/**
 * Permet de convertir un objet json en instance de classe Livre
 *
 * @export
 * @class LivreConverteur
 * @extends {MediaConverter}
 */
export class LivreConverteur extends MediaConverter {
    ISBN : string; // la saisie de l'iSBN sera donc du ressort de l'utilisateur final de l'application
    auteur : string;
    genre : string;
    maisonEdition : string;
    nbPage : number;

    constructor(_objectAConvertir) {
        super(_objectAConvertir);

        this.ISBN = _objectAConvertir.ISBN;
        this.auteur = _objectAConvertir.auteur;
        this.genre = _objectAConvertir.genre;
        this.maisonEdition = _objectAConvertir.maisonEdition;
        this.nbPage = _objectAConvertir.nbPage;
    }

    public conversionClasse() : Livre {
        return new Livre(this.id, this.ISBN, this.titre, this.nbExemplaires, this.auteur, this.genre, this.maisonEdition, this.nbPage)
    }
}

/**
 * Permet de convertir un objet json en instance de classe CD
 *
 * @export
 * @class CDConverteur
 * @extends {MediaConverter}
 */
export class CDConverteur extends MediaConverter {
    discID : string // Est ce que on le met en readonly comme l'isbn ? Oui même principe
    groupe : string;
    genre : string;
    producteur : string;
    dureeMn : number;

    constructor(_objectAConvertir) {
        super(_objectAConvertir);

        this.discID = _objectAConvertir.discID;
        this.groupe = _objectAConvertir.groupe;
        this.genre = _objectAConvertir.genre;
        this.producteur = _objectAConvertir.producteur;
        this.dureeMn = _objectAConvertir.dureeMn;
    }

    public conversionClasse() : CD {
        return new CD(this.id, this.discID, this.titre, this.nbExemplaires, this.groupe, this.genre, this.producteur, this.dureeMn)
    }
}

/**
 * Permet de convertir un objet json en instance de classe DVD
 *
 * @export
 * @class DVDConverteur
 * @extends {MediaConverter}
 */
export class DVDConverteur extends MediaConverter {
    EIRD : string;
    realisateur : string;
    genre : string;
    producteur : string;
    dureeMn : number;

    constructor(_objectAConvertir) {
        super(_objectAConvertir);

        this.EIRD = _objectAConvertir.EIRD;
        this.realisateur = _objectAConvertir.realisateur;
        this.genre = _objectAConvertir.genre;
        this.producteur = _objectAConvertir.producteur;
        this.dureeMn = _objectAConvertir.dureeMn;
    }

    public conversionClasse() : DVD {
        return new DVD(this.id, this.EIRD, this.titre, this.nbExemplaires, this.realisateur,this.genre, this.producteur, this.dureeMn);
    }
}

/**
 * Permet de convertir un objet json en instance de classe Emprunteur
 *
 * @export
 * @class EmprunteurConverteur
 */
export class EmprunteurConverteur {
    id : number;
    nom : string;
    prenom : string;
    email : string;

    constructor(_objectAConvertir) {

        this.id = _objectAConvertir.id;
        this.nom = _objectAConvertir.nom;
        this.prenom = _objectAConvertir.prenom;
        this.email = _objectAConvertir.email;
    }

    public conversionClasse() : Emprunteur {
        return new Emprunteur(this.id, this.nom, this.prenom, this.email);
    }
}

/**
 * Permet de convertir un objet json en instance de classe Emprunt
 *
 * @export
 * @class EmpruntConverteur
 */
export class EmpruntConverteur {
    id : number;
    idMedia : number;
    idEmprunteur : number;
    dateEmprunt : Date;
    dateRetour : Date;

    constructor(_objectAConvertir) {

        this.id = _objectAConvertir.id;
        this.idMedia = _objectAConvertir.idMedia;
        this.idEmprunteur = _objectAConvertir.idEmprunteur;
        this.dateEmprunt = new Date(_objectAConvertir.dateEmprunt);
        this.dateRetour = new Date(_objectAConvertir.dateRetour);
    }

    public conversionClasse() : Emprunt {
        return new Emprunt(this.id, this.idMedia, this.idEmprunteur, this.dateEmprunt, this.dateRetour);
    }
}