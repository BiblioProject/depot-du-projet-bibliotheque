/**
 * Interface permettant de s'assurer que les instances l'implémentant possèdent bien un identifiant et un indexPourrecherche
 *
 * @export
 * @interface Identifiable
 */
export interface Identifiable {
    id:number;
    indexPourRecherche:string;
}

/**
 * Interface permettant de s'assurer que la méthode ChargementDonneesObserver de l'observeur possède bien une méthode finished permettant le lancement de l'application
 *
 * @export
 * @interface ChargementDonneesObserver
 */
export interface ChargementDonneesObserver {
    finished();
}

/* design patern Iterateur non mis en place car utilisation de boucle for pour le parcours des collections
export interface Iterateur<T>{
    aUnSuivant(): boolean;
    suivant(): T;
}*/